#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 09:19:12 2022

@author: roncofaber
"""

# GPAW
import gpaw
from XAS_faber import XAS
import matplotlib.pyplot as plt

# other functions
import numpy as np
import glob
import os
import pickle

#%%

# dummy xasObj class to use to save XAS in bigger object
class xasObj():
    def __init__(self):
        return

class XAS_spectra():

    def __init__(
            self,
            basepath = ".",
            setpath  = ".",
            wfs_name = None,
            logfile  = "LOG",
            multiple = False,
            oname    = "default",
            save     = True,
            restart  = False,
            xch_spin = None,
            ):

        if not restart:
            self.__initialize_from_wavefunctions(basepath, setpath, wfs_name, logfile, multiple, oname, save,
                                                 xch_spin)
        else:
            self.__restart_XAS_from_pickle(basepath, restart)

        return

    # run XAS on new wfs file
    def __initialize_from_wavefunctions(self,
                                        basepath,
                                        setpath,
                                        wfs_name,
                                        logfile,
                                        multiple,
                                        oname,
                                        save,
                                        xch_spin
                                        ):
        # set attributes
        self.basepath = basepath
        self.setpath  = setpath
        self.wfs_name = wfs_name
        self.logfile  = logfile

        self.xch_spin = xch_spin

        # add setup path
        gpaw.setup_paths.insert(0, setpath)

        if not multiple:
            self.subpaths = [basepath]
        else:
            self.subpaths = self.__find_gpw_subdirectories()

        # generate list with all XAS
        self.xas_list = self.__generate_XAS_list()


        self.mean_dks = np.mean([xas.dks for xas in self.xas_list])

        # check energy range
        # self.Emin, self.Emax = self.__check_energy_range()

        # save object
        if save:
            self.__save_self(oname)

        return

    # restart XAS object from a pkl file
    def __restart_XAS_from_pickle(self, basepath, restart):

        # open previously generated gpw file
        with open(basepath + restart, "rb") as fin:
            restart = pickle.load(fin)

        self.__dict__ = restart.__dict__.copy()

        return

    # find all subdirectories with a gpw file and return as list
    def __find_gpw_subdirectories(self):

        gpw_files = glob.glob(self.basepath + "*/*.gpw", recursive=True)
        gpw_files.sort()

        subpaths = []

        for gpw_file in gpw_files:
            subpaths.append(os.path.dirname(gpw_file) + "/")

        return subpaths


    # generate a list of all XAS objects (one per excited atom)
    def __generate_XAS_list(self):

        xas_list = []
        for subpath in self.subpaths:
            xas = self.__read_single_XAS(subpath, self.wfs_name,
                                         self.logfile, self.xch_spin)
            xas_list.append(xas)

        return xas_list

    # read a single XAS folder
    def __read_single_XAS(self, path, wfs_name, logfile, xch_spin=None):

        # read wavefunctions
        calc = gpaw.GPAW(path + wfs_name)

        # read delta K-S energy
        with open(path + logfile, "r") as fin:
            for line in fin:
                if line.startswith("dks:"):
                    dks = float(line.split()[1])
                    break

        # crete a xas object
        xas = xasObj()

        # assign infos
        xas.dks = dks
        xas.calc_details = calc.todict()
        # if spinpol
        if xas.calc_details["spinpol"]:

            xas.up = XAS(calc, mode='xas', spin=0, xch_spin=xch_spin) #TODO
            xas.dn = XAS(calc, mode='xas', spin=1, xch_spin=xch_spin)
        else:
            xas.up = XAS(calc, mode='xas', xch_spin=xch_spin)

        return xas

    # # check all energy ranges of the XAS list and return the interval
    # def __check_energy_range(self):

    #     Emin =  np.inf
    #     Emax = -np.inf

    #     for xas in self.xas_list:

    #         Emin = np.minimum(Emin, min(xas.up.eps_n))
    #         Emax = np.maximum(Emax, max(xas.up.eps_n))

    #         if xas.calc_details["spinpol"]:
    #             Emin = np.minimum(Emin, min(xas.dn.eps_n))
    #             Emax = np.maximum(Emax, max(xas.dn.eps_n))

    #     Emin -= 0.2*(Emax-Emin)
    #     Emax += 0.2*(Emax-Emin)

    #     return Emin, Emax

    # return values of index idnr of the XAS spectra
    def get_single_XAS_spectra(self,
                               idnr,
                               spin   = "up", #default is spin up
                               weight = 1,
                               shift  = True
                               ):

        # select XAS spectra
        xas = self.xas_list[idnr]

        # get peak positions and intensities
        x_s, y_s = getattr(xas, spin).get_spectra(stick=True)

        # calculate total values
        y_tot_s = np.sum(y_s, axis=0)/3

        # calculate energy shift because of dks


        if shift:
            x_shift = xas.dks - x_s[0]
            return x_s + x_shift, weight*y_tot_s
        else:
            x_shift = xas.dks - x_s[0] - self.mean_dks
            return x_s + x_shift, weight*y_tot_s

    # get full xas spectra with all contributions
    def get_full_XAS_spectra(self,
                             spin    = "up",
                             npoints = 2001,
                             weights = None,
                             sigma   = 0.5,
                             shift   = True,
                             ):

        # get all contributions
        x_s_full = []
        y_s_full = []
        for cc, xas in enumerate(self.xas_list):

            if weights is not None:
                weight = weights[cc]
            else:
                weight = 1

            x_s, y_s = self.get_single_XAS_spectra(cc, spin, weight, shift)
            x_s_full.extend(x_s)
            y_s_full.extend(y_s)

        # define energy range
        Emin = np.min(x_s_full)
        Emax = np.max(x_s_full)
        dE   = np.abs(Emax - Emin)
        Emin -= 0.2*dE
        Emax += 0.2*dE

        E_range = np.linspace(Emin, Emax, npoints)

        # calculate broad spectra
        broad_spectra = self.broaden_spectrum(x_s_full, y_s_full, E_range, sigma)

        return E_range, np.array(x_s_full), np.array(y_s_full), broad_spectra



    # broaden spectrum to plot it
    def broaden_spectrum(self, x_peaks, y_peaks, energies, sigma=0.5):
        gaussian_spectrum = []

        gaussian_spectrum = np.zeros(len(energies))

        for x_p, y_p in zip(x_peaks, y_peaks):

            gaussian_spectrum += y_p*np.exp(-((((x_p - energies)/sigma)**2)))

        return gaussian_spectrum


    def __save_self(self, oname):

        # check output name
        if oname == "default":
            oname = "XAS_object.pkl"
        elif not oname.endswith(".pkl"):
            oname = oname.split(".")[0] + ".pkl"


        with open(self.basepath + oname, 'wb') as fout:
            pickle.dump(self, fout)

        print("Saved everything as {}".format(oname))
        return



