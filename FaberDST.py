#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
All the functions to work with Li-S systems:
    distortion
    find rings
    calculate position of Li wrt a S ring
    ...

Created on Tue Apr 13 09:30:01 2021

@author: roncofaber
"""
import ase.io
import ase.visualize
import FaberPAW as fap
import numpy as np
import os

mypath = os.path.dirname(os.path.abspath(__file__)) + "/"
#%%

# arrange the indexes of a sulfur ring so that it is cyclic
# neighbors are together

# input:
    # system: system where the S ring is
    # ring_indexes: indexes of the ring
def arrange_ring_indexes(system, ring_indexes):

    ri = ring_indexes.copy()
    ri.sort()

    reference_idx  = ri[0]
    arranged_idx = [reference_idx]
    idx_to_check = ri[1:]
    while len(idx_to_check) > 0:
        next_idx = idx_to_check[
            np.argsort(system.get_distances(reference_idx, idx_to_check, mic=True))[0]
            ]
        arranged_idx.append(next_idx)
        reference_idx = next_idx
        idx_to_check.remove(next_idx)

    return arranged_idx


# find sulphur ring given a single or multiple indexes of the ring
def find_sulphur_ring(index, system, arrange=True):
    S_indexes = np.where(np.array(system.get_chemical_symbols()) == "S")[0]

    if type(index) is int:
        ring_indexes = [index]
    else:
        ring_indexes = index.copy()

    tmp_idx = ring_indexes[-1]
    while len(ring_indexes) < 8:

        next_idx = S_indexes[np.argsort(system.get_distances(tmp_idx, S_indexes, mic=True))[:3]]

        for ii in next_idx:
            if ii not in ring_indexes:
                ring_indexes.append(ii)
                tmp_idx = ii
                continue

    if arrange:
        return arrange_ring_indexes(system, ring_indexes)
    else:
        return ring_indexes


# take a system with sulfur rings and label them (givin one id number to each ring)
def label_sulfur_system(system, pbc=[True, True, True]):

    system.pbc = pbc

    # take indexes of only the sulfur
    S_indexes = [atom.index for atom in system if atom.symbol == "S"]

    # initialize variables
    ring_list  = {}
    used_index = []

    cc = 1
    for index in S_indexes:

        if index in used_index: #index already used, ignore
            continue

        # find a new ring
        ring = find_sulphur_ring(index, system)

        # arrange indexes in a cyclic way
        ring = arrange_ring_indexes(system, ring)

        # update ring list
        ring_list[str(cc)] = ring
        used_index.extend(ring) # add used indexes to the list

        cc += 1

    return ring_list

# from a list of 8 S atoms of one ring, generate all possible couples
# where there can be a distortion
def generate_couples(indexes):
    return [[indexes[cc-1], indexes[cc]] for cc in range(len(indexes))]

# isolate a system with only one ring and the lithium, taking care of pbcs
def isolate_system(system, Li_idx, ring_indexes):

    tot_idx = ring_indexes.copy()
    tot_idx.insert(0, Li_idx)

    new_pos = system.get_positions()[tot_idx]

    # generate reference ring from system and indexes
    reference = ase.Atoms(symbols   = "LiS8",
                          positions = new_pos,
                          cell      = system.get_cell(),
                          pbc       = True
                          )

    vec = reference.get_all_distances(mic=True, vector=True)[1]
    new_cell = np.array([100, 100, 100])
    # print(new_cell)
    vec += new_cell/2

    reference.set_cell(np.diag(new_cell))
    reference.set_positions(vec)
    # take care of pbc issues

    return reference


# isolate a single sulfur ring
def isolate_sulphur_ring(system, ring_indexes):

    # generate reference ring from system and indexes
    reference = ase.Atoms(symbols   = "S8",
                          positions = system.get_positions()[ring_indexes],
                          cell      = system.get_cell(),
                          pbc       = True
                          )

    # take care of pbc issues
    abs_vec = reference.get_distances(0, range(8), vector=True)
    mic_vec = reference.get_distances(0, range(8), vector=True, mic=True)
    unwrap_translation = mic_vec - abs_vec
    reference.positions = reference.positions + unwrap_translation

    return reference


# get distortion vector given a reference state
def get_distortion_vector(reference):

    # Read initial and final states:
    name1 = mypath + "/files/POSCAR_bd_nosme"

    old_del_nos = ase.io.read(name1)  # reference delocalized
    old_del_shi = old_del_nos.copy() # reference but shifted of 1
    old_del_shi.positions = [old_del_shi.get_positions()[-1].tolist()] + old_del_shi.get_positions()[:-1].tolist()

    # map isolated ring on reference
    new_del_nos, __, __ = fap.rearrange_systems(reference, old_del_nos)
    new_del_shi, __, __ = fap.rearrange_systems(reference, old_del_shi)

    dist_nos = fap.get_distance_between_images([reference.get_positions(), new_del_nos.get_positions()])
    dist_shi = fap.get_distance_between_images([reference.get_positions(), new_del_shi.get_positions()])

    if dist_nos > dist_shi:
        name2 = mypath + "/files/POSCAR_b1_nosme"
        shifted = 1
        new_del = new_del_shi

    else:
        name2 = mypath + "/files/POSCAR_b-1_nosme"
        shifted = 0
        new_del = new_del_nos

    old_dis = ase.io.read(name2)
    old_dis.positions = old_dis.get_positions()[shifted:].tolist() + old_dis.get_positions()[:shifted].tolist()

    new_dis, __, __ = fap.rearrange_systems(new_del, old_dis)


    # calculate distortion vector
    dist_vec = new_dis.get_positions() - new_del.get_positions()

    return dist_vec


# apply distortion to a sulfur ring
def apply_distortion_S8ring(system, ring_indexes, write=False,
                            name="CONTCAR", opath=""):

    # check if ring indexes is not complete
    if len(ring_indexes) == 2:

        dist_idx = ring_indexes.copy()

        ring_indexes = find_sulphur_ring(ring_indexes, system, arrange=False)
        ring_indexes = ring_indexes[1:] + [ring_indexes[0]]

    else:
        print("specify distortion site, try again")
        return

    distortions = []
    for rindx in [ring_indexes, ring_indexes[::-1]]:
        reference = isolate_sulphur_ring(system, rindx)

        distortions.append(get_distortion_vector(reference))


    dist_vector = np.mean([distortions[0], distortions[1][::-1]], axis=0)

    # generate copy to distort system
    system_distorted = system.copy()
    system_distorted.positions[ring_indexes] = system_distorted.get_positions()[ring_indexes] + dist_vector

    # plot_systems(reference, arrows=dist_vec)

    # wrap em up
    system_distorted.wrap()

    # WRITE NICE LABELLED OUTPUT NOT SURE WHY THIS IS CAPS LOCK
    if write:

        ring_list = label_sulfur_system(system)
        myring = find_sulphur_ring(ring_indexes, system)

        rN = [ii for ii in ring_list if ring_list[ii] == myring][0]

        distortion_sites = generate_couples(ring_list[rN])

        dist_site = np.where([set(dist_idx) == set(ii) for ii in distortion_sites])[0][0]


        ase.io.write(opath + name + "_pol_R{}D{}".format(rN.zfill(2), str(dist_site).zfill(2)),
                     system_distorted, format="vasp", direct=True, vasp5=True,
                     ignore_constraints=True,
                     label="LiCS | ring: {} | dist: {}-{}".format(rN, *dist_idx))

    return system_distorted, reference



# find all rings within a cutoff distance of the Lithium
def find_rings_within_cutoff(structure, cutoff=8):

    Li_idx    = np.where(np.array(structure.get_chemical_symbols()) == "Li")[0][0]
    S_indexes = np.where(np.array(structure.get_chemical_symbols()) == "S")[0]


    distances = structure.get_distances(Li_idx, S_indexes, mic=True)

    r_cutoff = 8

    candidates = S_indexes[np.where(distances < r_cutoff)[0]]

    rings = []

    for cand in candidates:
        skip=False
        for ring in rings:
            if cand in ring:
                skip=True
        if skip:
            continue
        ri = find_sulphur_ring(cand, structure)
        rings.append(ri)

    isolated_systems = []
    for ring_indexes in rings:
        isolated_systems.append(isolate_system(structure, Li_idx, ring_indexes))

    return isolated_systems



# calculate coordination number
def calculate_CN(structure, atom1, atom2, d0, n=6, m=12):

    a1_idxs = [cc for cc, ii in enumerate(structure.get_chemical_symbols()) if ii == atom1]
    a2_idxs = [cc for cc, ii in enumerate(structure.get_chemical_symbols()) if ii == atom2]

    coordNum = []
    for idx in a1_idxs:
        dist = structure.get_distances(idx, a2_idxs, mic=True)

        up = 1 - (dist/d0)**n
        dn = 1 - (dist/d0)**m
        coordNum.append(np.sum(up/dn))
    return coordNum[0] if len(coordNum) == 1 else coordNum


#
def get_coordinates(structure, Li_index, ring_index, reference_S=None):


    def find_normal_to_ring(S_positions, COM):

        vectors = S_positions - COM

        vectors = np.append(vectors, [vectors[0]], axis=0)

        normals = []
        for cc, vec in enumerate(vectors[:-1]):
            normals.append(np.cross(vec, vectors[cc+1]))

        return np.sum(vectors, axis=0)

    def planeFit(points):
        """
        p, n = planeFit(points)

        Given an array, points, of shape (d,...)
        representing points in d-dimensional space,
        fit an d-dimensional plane to the points.
        Return a point, p, on the plane (the point-cloud centroid),
        and the normal, n.
        """
        points = np.reshape(points, (np.shape(points)[0], -1)) # Collapse trialing dimensions
        assert points.shape[0] <= points.shape[1], "There are only {} points in {} dimensions.".format(points.shape[1], points.shape[0])
        ctr = points.mean(axis=1)
        x = points - ctr[:,np.newaxis]
        M = np.dot(x, x.T) # Could also use np.cov(x) here.
        return ctr, np.linalg.svd(M)[0][:,-1]

    def project_on_plane(point, plane_point, plane_normal):
        return point - np.dot(point - plane_point, plane_normal) * plane_normal

    def angle_between(v1, v2):
        """ Returns the angle in radians between vectors 'v1' and 'v2'::
        """
        v1_u = v1/np.linalg.norm(v1)
        v2_u = v2/np.linalg.norm(v2)
        return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


    reference = isolate_system(structure, Li_index, ring_index)

    # print(reference)
    S_idx = [1,2,3,4,5,6,7,8]

    Li_pos = reference.get_positions()[0]
    S_pos = reference.get_positions()[1:]

    if reference_S is not None:
        closest_S = reference_S
    else:
        closest_S = [S_idx[ii] for ii in np.argsort(reference.get_distances(0, S_idx, mic=True))[:3]][0]


    # get com and normal to plane

    points_to_fit = np.append(S_pos, [np.mean(S_pos, axis=0)], axis=0)

    com, e1 = planeFit(points_to_fit.T)
    e1 = e1/np.linalg.norm(e1)

    # get e2
    pp = project_on_plane(reference.get_positions()[closest_S], com, e1)
    e2 = (pp - com)/np.linalg.norm(pp - com)


    if np.dot(e1, reference.get_positions()[closest_S] - pp) < 0:
        e1 = -e1

    # get e3
    e3 = np.cross(e1, e2)

    # find theta and phi and rad

    point_e2e3 = project_on_plane(Li_pos, com, e1)

    point_e1e2 = project_on_plane(Li_pos, com, e3)


    theta = angle_between(e1, Li_pos-com)#point_e1e2-com)
    phi   = angle_between(e2, point_e2e3-com)

    if np.dot(e3, Li_pos - point_e1e2) < 0:
        phi = -phi



    rad   = np.linalg.norm(Li_pos - com)

    return rad, 180*theta/np.pi, 180*phi/np.pi, closest_S







