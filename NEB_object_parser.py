#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 15:20:41 2021

@author: roncofaber
"""

# tools
import ase.io
from ase.io.trajectory import TrajectoryReader
import numpy as np

# parsing
import datetime
import subprocess
import glob

# plotting
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.animation import FuncAnimation


# parallel reader
import contextlib
import joblib
from joblib import Parallel, delayed, parallel_backend
import multiprocessing
from tqdm import tqdm

# other stuff
from FaberPAW import get_distance_between_images

sns.set()
sns.set_style("white")
#%%
@contextlib.contextmanager
def tqdm_joblib(tqdm_object):
    """Context manager to patch joblib to report into tqdm progress bar given as argument"""
    class TqdmBatchCompletionCallback(joblib.parallel.BatchCompletionCallBack):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def __call__(self, *args, **kwargs):
            tqdm_object.update(n=self.batch_size)
            return super().__call__(*args, **kwargs)

    old_batch_callback = joblib.parallel.BatchCompletionCallBack
    joblib.parallel.BatchCompletionCallBack = TqdmBatchCompletionCallback
    try:
        yield tqdm_object
    finally:
        joblib.parallel.BatchCompletionCallBack = old_batch_callback
        tqdm_object.close()

#%% generate NEB object with attributes

class NEB_object():
    def __init__(self, directory, scfname=None, nebname=None, ignore_extrema=True):

        self.directory = directory

        if nebname is None:
            self.nebname = "neb_relaxation.traj"
        else:
            self.nebname = nebname

        if scfname is None:
            self.scfname = "scf*.out"
        else:
            self.scfname = scfname

        # get list of scf filenames and number of images
        self.filenames, self.nimages = self.get_neb_files_names()

        # if image 00 and -1 are not calculated (normal case), add two to total images
        if ignore_extrema:
            self.nimages = self.nimages + 2

        # read neb trajectory and save it
        self.neb_trajectory = self.read_neb_trajectory()

        # read all possible energies
        self.energies = self.get_trajectories_energy()

        self.delta_energies = 1000*(self.energies - np.nanmin(self.energies))


        # try:
        #     self.time, self.forces, self.nsteps = self.get_time_performance()
        # except:
        #     self.nsteps = 1

        self.positions, self.positions_list = self.track_displacement()

        # self.tenergy, self.tdE = self.get_energy_over_time()


    def get_neb_files_names(self):

        filenames = glob.glob(self.directory + self.scfname)
        nimages   = len(filenames)

        return sorted(filenames), nimages

    def read_neb_trajectory(self):

        filein = self.directory + self.nebname

        complete_trajectory = ase.io.read(filein + "@:")

        neb_trajectory = []
        for cc in range(self.nimages):
            neb_trajectory.append(complete_trajectory[cc*self.nimages:(cc+1)*self.nimages])

        return neb_trajectory


    def get_trajectories_energy(self):

        energies = []

        for snippet in self.neb_trajectory:
            energy = []
            for img in snippet:
                try:
                    energy.append(img.get_potential_energy())
                except:
                    energy.append(np.NaN)
            energies.append(energy)

        return np.array(energies)






    # read gparser and return energies of NEB
    def get_energy_barrier(self):

        def parse_energy(filename):
            linetoparse = "gparser.py -i {} -cfo".format(filename)

            command = subprocess.run(linetoparse,
                                    shell=True,
                                    cwd=self.directory,
                                    stdout=subprocess.PIPE)

            energies = command.stdout.decode("utf-8").strip().split("\n")

            print(command.stdout)

            outlist = []
            for item in energies:
                if not item:
                    print("found a crappy item")
                elif item.startswith("np.NaN"):
                    # print("here nan")
                    outlist.append(np.NaN)
                else:
                    # print("just here")
                    outlist.append(float(item))

            print("parsed {}".format(filename))

            return outlist

        #optimize division of work to not overload cores
        def choose_number_jobs(worksize, num_cores):
            ratio = worksize/num_cores
            n = 1
            while ratio > 1:
                n += 1
                ratio = worksize/(n*num_cores)

            njobs = int(np.ceil(worksize/n))

            if njobs == 0:
                njobs = 1

            return njobs

        # optimize resource usage
        ncpus   = int(multiprocessing.cpu_count()/2)
        totjobs = len(self.filenames)
        njobs = ncpus#choose_number_jobs(totjobs, ncpus)

        # parallel run to generate all normalized data
        with tqdm_joblib(tqdm(desc="Reading files in parallel:",
                              total=totjobs)):
            with parallel_backend("loky", inner_max_num_threads=2):
                energies_list = Parallel(n_jobs=njobs)(delayed(parse_energy)(
                    filename) for filename in self.filenames)

        print("Elists:")
        print(energies_list)
        smallest_list = np.min([len(ii) for ii in energies_list])
        energies_list = np.array([ii[:smallest_list] for ii in energies_list])



        return energies_list, energies_list[:,-1]


    # read times and number of steps required to perform NEB
    def get_time_performance(self):

        times  = []
        forces = []
        with open(self.directory + "LOG", "r") as fin:
            next(fin)
            for line in fin:

                def check_line_start(line):
                    if line.startswith("MDMin"):
                        return True
                    elif line.startswith("FIRE"):
                        return True
                    else:
                        return False

                if check_line_start(line):

                    time = line.split()[2]
                    force = line.split()[4]
                    times.append(datetime.datetime.strptime(time, '%H:%M:%S'))
                    forces.append(float(force))

        return (times[-1] - times[0]).seconds, forces, len(times)

    # track trajectory of each atom
    def track_displacement(self):


        system = TrajectoryReader(self.directory + self.nebname)


        positions = []
        positions_list = []
        for cc, pos in enumerate(system):

            positions.append(pos.positions)

            if (cc+1)%(self.nimages+2) == 0:
                positions_list.append(positions)
                positions = []

        return positions_list[-1], positions_list

    def get_position_along_path(self, pos, atonr):

        N     = self.nimages if not self.final else self.nimages + 2
        shift = 0 if self.final else 1

        if atonr is None:
            return np.linspace(0, 1, N)

        dist = [0]
        for cc, pp in enumerate(pos[1+shift:-shift]):

            dist.append(dist[-1] + np.linalg.norm(pp - pos[cc]))

        return np.array(dist)/np.max(dist)

    def plot_energy_over_time(self, skipsteps):
        fs = 13
        ms = 7

        col = plt.cm.viridis(np.linspace(0,1,np.shape(self.tenergy)[1]))

        fig, ax1 = plt.subplots(figsize=(7,5))



        minenergy = np.nanmin([np.nanmin(energy) for energy in self.tenergy.T])


        for cc, energy in enumerate(self.tenergy.T):

            if cc%skipsteps:
                continue

            de = energy - minenergy

            ax1.plot(
                    np.linspace(0, 1, len(de)),
                    1000*de,
                    color=col[cc],
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    # label=r"{}".format(self.subdirs[cc])
                    )


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        # plt.legend()
        plt.tight_layout()
        plt.show()


    def plot_force_evolution(self):
        fs = 13
        ms = 7

        fig, ax1 = plt.subplots(figsize=(7,5))

        ax1.plot(
                range(self.nsteps),
                self.forces,
                linestyle='--',
                marker='o',
                markersize=ms,
                # label=r"{}".format(self.subdirs[cc])
                )


        ax1.set_xlabel(r'step number [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'av. force [eV/$\AA$]', fontsize=fs, fontweight="bold")

        # plt.legend()
        plt.tight_layout()
        plt.show()

    def plot_positions_over_time(self, atonr):
        fs = 13
        ms = 7

        def track_xz(frame, atonr):
            xpos = []
            zpos = []

            for img in frame:
                cpos = img[atonr]

                xpos.append(cpos[0])
                zpos.append(cpos[2])

            return [xpos, zpos]

        col = plt.cm.viridis(np.linspace(0,1,np.shape(self.tenergy)[1]+1))

        fig, ax1 = plt.subplots(figsize=(6,6))
        ax1.axis("equal")

        for cc, frame in enumerate(self.positions_list):

            cpos = track_xz(frame, atonr)

            ax1.plot(
                    cpos[0],
                    cpos[1],
                    color=col[cc],
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    # label=r"{}".format(self.subdirs[cc])
                    )


        ax1.set_xlabel(r'x coord [$\AA$]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'z coord [$\AA$]', fontsize=fs, fontweight="bold")

        plt.title("Trajectory of atom #{}".format(atonr+1))
        plt.tight_layout()
        plt.show()

    def plot_images_over_time(self, atonr):
        fs = 13
        ms = 3

        allpos = np.array(self.positions_list)

        all_x_pos = allpos[:,:,atonr,0].T
        all_z_pos = allpos[:,:,atonr,2].T

        fig, ax1 = plt.subplots(figsize=(6,6))
        ax1.axis("equal")

        for cc in range(self.nimages+2):

            ax1.plot(
                    all_x_pos[cc,:],
                    all_z_pos[cc,:],
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    # label=r"{}".format(self.subdirs[cc])
                    )


        ax1.set_xlabel(r'x coord [$\AA$]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'z coord [$\AA$]', fontsize=fs, fontweight="bold")

        plt.title("Trajectory of atom #{}".format(atonr+1))
        plt.tight_layout()
        plt.show()


    def animate_energy_barrier(self, atonr=None, save=False):


        def generate_frames(df1, pos_list, atonr):

            xdata = [self.get_position_along_path(pos, atonr) for pos in pos_list]
            ydata = [1000*(en - np.nanmin(en)) for en in df1]

            return xdata, ydata


        xdata, ydata = generate_frames(self.tenergy.T, self.positions_list, atonr)

        ymax = [np.max(y) for y in ydata]

        fs = 13
        ms = 7

        # plt.style.use('seaborn-pastel')

        fig, ax1 = plt.subplots(figsize=(7,5))

        ax1.set_xlim([0,1])
        ax1.set_ylim([0,ymax[0]])

        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        line, = ax1.plot([], [], lw=2,
                          color="b",
                          linestyle='-',
                          marker='o',
                          markersize=ms)

        def init():
            line.set_data([], [])
            return line,

        def animate(ii):
            # print("here")
            line.set_data(xdata[ii], ydata[ii])
            ax1.set_ylim([0,1.03*ymax[ii]])
            return line,




        anim = FuncAnimation(fig, animate,
                              init_func=init,
                             frames=len(ydata),
                             interval=15,
                             blit=False)

        if save:
            anim.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264',
                                                                 '-threads', '0',
                                                                 ])
        # plt.show()
        return anim

    def plot_energy_barrier(self, atonr=None, save=False,
                            oname="NEB_energy_barrier"):
        fs = 13
        ms = 7

        fig, ax1 = plt.subplots(figsize=(7,5))

        if atonr is None:
            dist = range(len(self.energy))
        else:
            if atonr == 'all':
                atonr = None
            dist = [0]
            dist.extend(get_distance_between_images(self.positions, atonr))

            dist = np.cumsum(np.array(dist)/np.sum(dist))


        ax1.plot(
                dist,
                1000*(self.energy - np.nanmin(self.energy)),
                # color='r',
                linestyle='--',
                marker='o',
                markersize=ms)

        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        plt.legend()
        plt.tight_layout()

        plt.show()

        if save:
            plt.savefig(oname + ".pdf", transparent=True)


class multiNEB():



    def __init__(self, rootdir, subdirs, scfname=None, nebname=None, final=False):

        if nebname is None:
            self.nebname = "neb_relaxation.traj"
        else:
            self.nebname = nebname

        if scfname is None:
            self.scfname = "scf*.out"
        else:
            self.scfname = scfname

        self.final   = final
        self.rootdir = rootdir
        self.subdirs = subdirs


        self.NEBobj_list = self.generate_lists()


    def generate_lists(self):



        NEBobj_list = []

        for mydir in self.subdirs:

            NEBobj = NEB_object(self.rootdir + mydir,
                                scfname=self.scfname, nebname=self.nebname,
                                final = self.final)

            NEBobj_list.append(NEBobj)

        return NEBobj_list

    def plot_energy_barriers(self, atonr=None, xcoords=None, save=False,
                             oname="NEB_energy_barrier"):
        fs = 13
        ms = 7

        # get total energy minima
        energymin = np.min([np.nanmin(obj.energy) for obj in self.NEBobj_list])

        fig, ax1 = plt.subplots(figsize=(7,5))

        for cc, obj in enumerate(self.NEBobj_list):

            dist = [0]
            dist.extend(get_distance_between_images(obj.positions, atonr))

            dist = np.cumsum(np.array(dist)/np.sum(dist))


            if xcoords is not None:
                cr = xcoords[cc]
                dist = dist * (cr[1] - cr[0]) + cr[0]


            ax1.plot(
                    dist,
                    1000*(obj.energy - energymin),
                    # color='r',
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    label=r"{}".format(self.subdirs[cc]))


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        plt.legend()
        plt.tight_layout()

        plt.show()

        if save:

            plt.savefig(oname + ".pdf", transparent=True)





    def plot_performance(self):
        fs = 13

        fig, ax1 = plt.subplots(figsize=(7,5))

        barpos = np.arange(len(self.subdirs))
        ax1.bar(barpos,
                [obj.time/3600 for obj in self.NEBobj_list],
                edgecolor='black')


        plt.xticks(barpos, self.subdirs)


        ax1.set_xlabel(r'method used', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'simulation time [hours]', fontsize=fs, fontweight="bold")

        plt.tight_layout()
        plt.show()

    def plot_displacement(self, atonr):
        fs = 13
        ms = 7
        def track_xz(pos, atonr):
            xpos = []
            zpos = []

            for ii in pos:
                cpos = ii[atonr]

                xpos.append(cpos[0])
                zpos.append(cpos[2])

            return [xpos, zpos]



        fig, ax1 = plt.subplots(figsize=(6,6))
        ax1.axis("equal")
        for cc,obj in enumerate(self.NEBobj_list):

            tpos = obj.positions

            xpos, zpos = track_xz(tpos, atonr)
            ax1.plot(xpos,
                    zpos,
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    label=r"{}".format(self.subdirs[cc]))

        # ax1.set_xlim([10.6,11.3])
        # ax1.set_ylim([8.175, 8.875])

        ax1.set_xlabel(r'x coord [$\AA$]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'z coord [$\AA$]', fontsize=fs, fontweight="bold")

        plt.title("Trajectory of S #{} for different k values".format(atonr+1))
        plt.legend()
        plt.tight_layout()
        plt.show()




        return



