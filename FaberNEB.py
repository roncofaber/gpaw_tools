#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 15:20:41 2021

@author: roncofaber
"""

# tools
import ase.io
import numpy as np

# parsing
import datetime
import subprocess
import glob

# plotting
import matplotlib.pyplot as plt
import seaborn as sns


# parallel reader
import contextlib
import joblib
from joblib import Parallel, delayed, parallel_backend
import multiprocessing
from tqdm import tqdm

# other stuff
from FaberPAW import get_distance_between_images

sns.set()
sns.set_style("white")
#%%
@contextlib.contextmanager
def tqdm_joblib(tqdm_object):
    """Context manager to patch joblib to report into tqdm progress bar given as argument"""
    class TqdmBatchCompletionCallback(joblib.parallel.BatchCompletionCallBack):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def __call__(self, *args, **kwargs):
            tqdm_object.update(n=self.batch_size)
            return super().__call__(*args, **kwargs)

    old_batch_callback = joblib.parallel.BatchCompletionCallBack
    joblib.parallel.BatchCompletionCallBack = TqdmBatchCompletionCallback
    try:
        yield tqdm_object
    finally:
        joblib.parallel.BatchCompletionCallBack = old_batch_callback
        tqdm_object.close()

#%% generate NEB object with attributes

class NEB_object():
    def __init__(self, directory, scfname=None, nebname=None, ignore_extrema=True,
                 logfile=None):

        self.directory = directory

        if nebname is None:
            self.nebname = "neb_relaxation.traj"
        else:
            self.nebname = nebname

        if scfname is None:
            self.scfname = "scf*.out"
        else:
            self.scfname = scfname

        # get list of scf filenames and number of images
        self.filenames, self.nimages = self.get_neb_files_names()

        # if image 00 and -1 are not calculated (normal case), add two to total images
        if ignore_extrema:
            self.nimages = self.nimages + 2

        # read neb trajectory and save it
        self.neb_trajectory = self.read_neb_trajectory()

        # read all possible energies
        self.energies = self.get_trajectory_energy()

        # calculate dE
        self.delta_energies = 1000*(self.energies - np.nanmin(self.energies))

        # get positions
        self.positions = self.get_positions_from_trajectory()

        # if logfile specified, read it
        if logfile is not None:
            self.logfile = logfile
            self.time, self.forces, self.nsteps = self.get_time_performance()

    # read the NEB files (scf*, cdft*)
    def get_neb_files_names(self):

        filenames = glob.glob(self.directory + self.scfname)
        nimages   = len(filenames)

        return sorted(filenames), nimages

    # read trajectory file and refactor it
    def read_neb_trajectory(self):

        filein = self.directory + self.nebname

        complete_trajectory = ase.io.read(filein + "@:")

        neb_trajectory = []
        for cc in range(int(len(complete_trajectory)/self.nimages)):
            neb_trajectory.append(complete_trajectory[cc*self.nimages:(cc+1)*self.nimages])

        return neb_trajectory

    # read energies from trajectory
    def get_trajectory_energy(self):

        energies = []

        for snippet in self.neb_trajectory:
            energy = []
            for img in snippet:
                try:
                    energy.append(img.get_potential_energy())
                except:
                    energy.append(np.NaN)
            energies.append(energy)

        return np.array(energies)

    # get positions from trajectory
    def get_positions_from_trajectory(self):

        positions = []

        for snippet in self.neb_trajectory:
            frame = []
            for img in snippet:
                frame.append(img.get_positions())
            positions.append(frame)

        return positions


    def plot_energy_over_time(self, skipsteps):
        fs = 13
        ms = 7

        col = plt.cm.viridis(np.linspace(0,1,len(self.energies)))

        fig, ax1 = plt.subplots(figsize=(7,5))

        for cc, dE in enumerate(self.delta_energies):

            if cc%skipsteps:
                continue

            ax1.plot(
                    np.linspace(0, 1, len(dE)),
                    dE,
                    color=col[cc],
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    # label=r"{}".format(self.subdirs[cc])
                    )


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        # plt.legend()
        plt.tight_layout()
        plt.show()


    def plot_force_evolution(self):
        fs = 13
        ms = 7

        fig, ax1 = plt.subplots(figsize=(7,5))

        ax1.plot(
                range(self.nsteps),
                self.forces,
                linestyle='--',
                marker='o',
                markersize=ms,
                # label=r"{}".format(self.subdirs[cc])
                )


        ax1.set_xlabel(r'step number [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'av. force [eV/$\AA$]', fontsize=fs, fontweight="bold")

        # plt.legend()
        plt.tight_layout()
        plt.show()

    def plot_positions_over_time(self, atonr):
        fs = 13
        ms = 7

        def track_xz(frame, atonr):
            xpos = []
            zpos = []

            for img in frame:
                cpos = img[atonr]

                xpos.append(cpos[0])
                zpos.append(cpos[2])

            return [xpos, zpos]

        col = plt.cm.viridis(np.linspace(0,1,np.shape(self.tenergy)[1]+1))

        fig, ax1 = plt.subplots(figsize=(6,6))
        ax1.axis("equal")

        for cc, frame in enumerate(self.positions_list):

            cpos = track_xz(frame, atonr)

            ax1.plot(
                    cpos[0],
                    cpos[1],
                    color=col[cc],
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    # label=r"{}".format(self.subdirs[cc])
                    )


        ax1.set_xlabel(r'x coord [$\AA$]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'z coord [$\AA$]', fontsize=fs, fontweight="bold")

        plt.title("Trajectory of atom #{}".format(atonr+1))
        plt.tight_layout()
        plt.show()


    def plot_energy_barrier(self, atonr=None, save=False,
                            oname="NEB_energy_barrier"):
        fs = 13
        ms = 7

        fig, ax1 = plt.subplots(figsize=(7,5))

        if atonr is None:
            dist = range(len(self.energies[-1]))
        else:
            if atonr == 'all':
                atonr = None
            dist = [0]
            dist.extend(get_distance_between_images(self.positions[-1], atonr))

            dist = np.cumsum(np.array(dist)/np.sum(dist))


        ax1.plot(
                dist,
                self.delta_energies[-1],
                # color='r',
                linestyle='--',
                marker='o',
                markersize=ms)

        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        plt.legend()
        plt.tight_layout()

        plt.show()

        if save:
            plt.savefig(oname + ".pdf", transparent=True)


class multiNEB():



    def __init__(self, rootdir, subdirs, scfname=None, nebname=None, final=False):

        if nebname is None:
            self.nebname = "neb_relaxation.traj"
        else:
            self.nebname = nebname

        if scfname is None:
            self.scfname = "scf*.out"
        else:
            self.scfname = scfname

        self.final   = final
        self.rootdir = rootdir
        self.subdirs = subdirs


        self.NEBobj_list = self.generate_lists()


    def generate_lists(self):



        NEBobj_list = []

        for mydir in self.subdirs:

            NEBobj = NEB_object(self.rootdir + mydir,
                                scfname=self.scfname, nebname=self.nebname,
                                final = self.final)

            NEBobj_list.append(NEBobj)

        return NEBobj_list

    def plot_energy_barriers(self, atonr=None, xcoords=None, save=False,
                             oname="NEB_energy_barrier"):
        fs = 13
        ms = 7

        # get total energy minima
        energymin = np.min([np.nanmin(obj.energy) for obj in self.NEBobj_list])

        fig, ax1 = plt.subplots(figsize=(7,5))

        for cc, obj in enumerate(self.NEBobj_list):

            dist = [0]
            dist.extend(get_distance_between_images(obj.positions, atonr))

            dist = np.cumsum(np.array(dist)/np.sum(dist))


            if xcoords is not None:
                cr = xcoords[cc]
                dist = dist * (cr[1] - cr[0]) + cr[0]


            ax1.plot(
                    dist,
                    1000*(obj.energy - energymin),
                    # color='r',
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    label=r"{}".format(self.subdirs[cc]))


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        plt.legend()
        plt.tight_layout()

        plt.show()

        if save:

            plt.savefig(oname + ".pdf", transparent=True)





    def plot_performance(self):
        fs = 13

        fig, ax1 = plt.subplots(figsize=(7,5))

        barpos = np.arange(len(self.subdirs))
        ax1.bar(barpos,
                [obj.time/3600 for obj in self.NEBobj_list],
                edgecolor='black')


        plt.xticks(barpos, self.subdirs)


        ax1.set_xlabel(r'method used', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'simulation time [hours]', fontsize=fs, fontweight="bold")

        plt.tight_layout()
        plt.show()

    def plot_displacement(self, atonr):
        fs = 13
        ms = 7
        def track_xz(pos, atonr):
            xpos = []
            zpos = []

            for ii in pos:
                cpos = ii[atonr]

                xpos.append(cpos[0])
                zpos.append(cpos[2])

            return [xpos, zpos]



        fig, ax1 = plt.subplots(figsize=(6,6))
        ax1.axis("equal")
        for cc,obj in enumerate(self.NEBobj_list):

            tpos = obj.positions

            xpos, zpos = track_xz(tpos, atonr)
            ax1.plot(xpos,
                    zpos,
                    linestyle='--',
                    marker='o',
                    markersize=ms,
                    label=r"{}".format(self.subdirs[cc]))

        # ax1.set_xlim([10.6,11.3])
        # ax1.set_ylim([8.175, 8.875])

        ax1.set_xlabel(r'x coord [$\AA$]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'z coord [$\AA$]', fontsize=fs, fontweight="bold")

        plt.title("Trajectory of S #{} for different k values".format(atonr+1))
        plt.legend()
        plt.tight_layout()
        plt.show()




        return



