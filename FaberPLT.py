#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 17:44:11 2021

@author: roncofaber
"""
import numpy as np
import matplotlib.pyplot as plt
import glob
import seaborn as sns
import scipy.integrate as spint
#%%

def get_efermi_from_dosfile(dosfile):
    with open(dosfile, "r") as fin:
        for line in fin:
            if line.startswith("# Efermi"):
                efermi = line.strip().split()[2]

                break
    return float(efermi)

def calculate_efermi_from_dos(dosdata, nelect, nbins=2001):
    enup  = np.array(dosdata[0])
    dosup = np.array(dosdata[1])
    endn  = np.array(dosdata[2])
    dosdn = np.array(dosdata[3])

    emin = np.min([enup, endn])
    emax = np.max([enup, endn])
    erange = np.linspace(emin, emax, nbins)

    cdos = []
    for en in erange:

        tmpdos = 0
        for cc, energy in enumerate([enup, endn]):

            idx = energy <= en

            if any(idx):
                tmpdos += spint.cumtrapz([dosup, dosdn][cc][idx],
                                         energy[idx],
                                         initial=0)[-1]

        cdos.append(round(tmpdos, 3))

    cdos = np.array(cdos)

    fermi_index = np.where(cdos>=nelect)[0][0]

    efermi = erange[fermi_index]

    return efermi

def read_dosfiles_spinpol(dos_prefix, path=""):

    dosdata = []
    efermi  = []
    for spin in ["up", "dn"]:

        dosfile = path + dos_prefix + "_{}.dat".format(spin)

        # read dosfile
        data = np.loadtxt(dosfile).T.tolist()
        # data vector with 2 energies and 2 dos (up/dn)
        dosdata.extend(data)

        efermi.append(get_efermi_from_dosfile(dosfile))

    return dosdata, efermi


def get_multiple_DOS(path, subdirs, dos_prefix, nelect=None, shift_efermi=False,
                     nbins=2001, hist=False):

    doslist  = []
    efermis  = []

    for cdir in subdirs:

        efermi  = []
        dosdata = []

        # read dosdata
        dosdata, efermi = read_dosfiles_spinpol(dos_prefix, path=path + cdir)

        if nelect is not None:
            ef = calculate_efermi_from_dos(dosdata, nelect)
            efermi = [ef, ef]

        efermis.append(efermi)
        doslist.append(np.array(dosdata).T)

    # make them arrays, much easier to work with
    doslist = np.array(doslist)
    efermis = np.array(efermis)

    # extract all data
    enerup = doslist[:,:,0]
    enerdn = doslist[:,:,2]
    updos  = doslist[:,:,1]
    dndos  = doslist[:,:,3]

    # shift with respect to efermi
    # if shift_efermi:
    #    enerup  -= efermis[:,0].T[:,None]
    #    enerdn  -= efermis[:,1].T[:,None]
    #    efermis -= efermis

    if hist:
        emin = np.min(np.min([enerup, enerdn]))
        emax = np.max(np.max([enerup, enerdn]))

        xmax = len(subdirs)

        b_dos_up = []
        b_dos_dn = []
        for cc in range(xmax):

            histup = np.histogram(enerup[cc],
                                  range=(emin, emax),
                                  bins=nbins,
                                  weights=updos[cc])

            histdn = np.histogram(enerdn[cc],
                          range=(emin, emax),
                          bins=nbins,
                          weights=dndos[cc])

            b_dos_up.append(histup[0])
            b_dos_dn.append(histdn[0])

        energies = len(b_dos_up)*[2*[(histup[1][:-1] + histup[1][1:])/2]]
        b_dos_up = np.array(b_dos_up)
        b_dos_dn = np.array(b_dos_dn)

    else:

        energies = [[enerup[cc], enerdn[cc]] for cc in range(len(enerup))]
        b_dos_up = np.array(updos)
        b_dos_dn = np.array(dndos)


    return energies, efermis, b_dos_up, b_dos_dn

def plot_DOS(energies, dos_up, dos_dn,
             efermis  = None,
             spinpol  = True,
             emin     = -3,
             emax     = 3,
             title    = None,
             shift_efermi = False,
             labels       = None,
             dosmin = -40,
             dosmax = 40):



    fig, ax1 = plt.subplots(figsize=(7,5))

    for cc, energy in enumerate(energies):

        color = sns.color_palette()[cc]

        if shift_efermi:
            eshift = efermis[cc][0]
        else:
            eshift = 0

        for spin, dos in enumerate([dos_up[cc], dos_dn[cc]]):

            ax1.plot(energy[spin] - eshift, [1 if spin else -1]*dos,
                      color = color, label="_nolegend_" if spin else None)

            if efermis is not None:

                if not shift_efermi:

                    if spin:
                        ax1.axvline(x=efermis[cc][spin]-eshift, ymin=0.5, color=color, linestyle="--", label='_nolegend_')
                    else:
                        ax1.axvline(x=efermis[cc][spin]-eshift, ymax=0.5, color=color, linestyle="--", label='_nolegend_')
                else:
                    ax1.axvline(x=0, color="k", linestyle="--", label='_nolegend_')

    if labels is not None:
        plt.legend(labels)

    if efermis is not None:
        ax1.set_xlim(left=np.mean(efermis) + emin - eshift, right= np.mean(efermis) + emax - eshift)
    ax1.set_ylim(bottom=dosmin, top=dosmax)
    ax1.set_xlabel("Energy [eV]")
    ax1.set_ylabel("DOS [-]")
    plt.title(title)
    plt.tight_layout()
    plt.show()

def plot_DOS_heatmap(energies, dos_up, dos_dn,
                     efermis      = None,
                     title        = None,
                     xlabel       = None,
                     xticlb       = None):

    fs = 12
    xmax = np.shape(dos_up)[0]

    emin = np.min(energies)
    emax = np.max(energies)

    xticks  = [cc+0.5 for cc in range(xmax)]
    xlabels = xticlb

    if dos_up is not None and dos_dn is not None:
        dosdata = [dos_up, dos_dn]
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12,6), sharey=True)
        myaxes = [ax1, ax2]
        spinlabels = ["Spin up", "Spin dn"]

    else:
        dosdata = [dos_up if dos_up is not None else dos_dn]
        fig, ax1 = plt.subplots(figsize=(6,6))
        myaxes = [ax1]
        spinlabels = ["Spin up" if dos_up is not None else "Spin dn"]

    if title is not None:
        fig.suptitle(title, fontsize=fs+2,  fontweight="bold")

    for cc, ax in enumerate(myaxes):
        ax.imshow(dosdata[cc].T, origin='lower', cmap="mako", aspect="auto",
           interpolation='none', extent=(0, xmax, emin, emax))

        # add white lines and efermi
        for section in range(xmax):
            ax.axvline(x=section, color="w", linewidth=0.8)

            if efermis is not None:
                ax.axhline(efermis[section][cc], xmin=section/xmax,
                           xmax=(section+1)/xmax, linewidth=0.75, color='r', linestyle="--")

        if efermis is None:
            ax.axhline(0, linewidth=0.75, color='r', linestyle="--")
        ax.text(xmax, 0, r"  E$_f$", fontsize=fs, fontweight="bold")

        ax.set_ylim(bottom=-5, top=2)

        ax.set_xlabel(xlabel, fontsize=fs)
        ax.set_ylabel("Energy [eV]", fontsize=fs)
        ax.set_title(spinlabels[cc], fontsize=fs)

        if xticlb is not None:
            ax.set_xticks(xticks)
            ax.set_xticklabels(xlabels)



    plt.tight_layout()
    plt.show()

    return