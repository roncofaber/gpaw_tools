#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 24 11:31:20 2022

@author: roncoroni
"""

# ASE
import ase
import ase.visualize

# other
import gpaw_tools.FaberPAW as fap
import numpy as np
import copy

# os
import copy
# path of current file (need to distort polaron)
import os
file_path = os.path.dirname(os.path.abspath(__file__)) + "/"
#%% aux functions

def as_list(inp):
    return [inp] if isinstance(inp, int) else inp.copy()

# return list of indexes from mixed input of indexes and string (elements)
def atoms_to_indexes(system, symbols):

    # check if symbols is a list of strings
    if symbols == "all":
        return list(range(len(system.get_chemical_symbols())))

    if not isinstance(symbols, list):
        symbols = [symbols]

    indexes = []
    for symbol in symbols:
        if not isinstance(symbol, str):
            indexes.append(symbol)
        else:
            for cc, atom in enumerate(system.get_chemical_symbols()):
                if atom == symbol:
                    indexes.append(cc)

    return indexes


# get distortion vector given a reference state
def get_distortion_vector(reference):

    # Read initial and final states:
    name1 = file_path + "/files/POSCAR_bd_nosme"
    # name1 = file_path + "/files/POSCAR_S8_symmetric"

    old_del_nos = ase.io.read(name1)  # reference delocalized
    old_del_shi = old_del_nos.copy() # reference but shifted of 1
    old_del_shi.positions = [old_del_shi.get_positions()[-1].tolist()] + old_del_shi.get_positions()[:-1].tolist()

    # map isolated ring on reference
    new_del_nos, __, __ = fap.rearrange_systems(reference, old_del_nos)
    new_del_shi, __, __ = fap.rearrange_systems(reference, old_del_shi)

    dist_nos = fap.get_distance_between_images([reference.get_positions(), new_del_nos.get_positions()])
    dist_shi = fap.get_distance_between_images([reference.get_positions(), new_del_shi.get_positions()])

    if dist_nos > dist_shi:
        name2 = file_path + "/files/POSCAR_b1_nosme"
        shifted = 1
        new_del = new_del_shi

    else:
        name2 = file_path + "/files/POSCAR_b-1_nosme"
        shifted = 0
        new_del = new_del_nos

    old_dis = ase.io.read(name2)
    old_dis.positions = old_dis.get_positions()[shifted:].tolist() + old_dis.get_positions()[:shifted].tolist()

    new_dis, __, __ = fap.rearrange_systems(new_del, old_dis)


    # calculate distortion vector
    dist_vec = new_dis.get_positions() - new_del.get_positions()

    return dist_vec

# isolate a single sulfur ring
def isolate_sulphur_ring(system, ring_indexes):

    reference = system[ring_indexes]

    # take care of pbc issues
    abs_vec = reference.get_distances(0, range(8), vector=True)
    mic_vec = reference.get_distances(0, range(8), vector=True, mic=True)
    unwrap_translation = mic_vec - abs_vec
    reference.positions = reference.positions + unwrap_translation

    return reference

# from a list of 8 S atoms of one ring, generate all possible couples
# where there can be a distortion
def generate_couples(indexes):
    return [[indexes[cc-1], indexes[cc]] for cc in range(len(indexes))]

# check that a list contains another one
def dist_in_ring(dist_site, ring_idx):

    set1 = set(dist_site)
    set2 = set(ring_idx)
    if len(set1.intersection(set2)) == 2:
        return True
    else:
        return False

#%% MAIN CLASS

# Class for single ring
class SulfurRing(ase.Atoms):
    def __init__(self, system, ring_indexes):

        assert len(ring_indexes) == 8, "Check that indexes has length 8"

        super().__init__(isolate_sulphur_ring(system, ring_indexes))


        self.indexes = ring_indexes
        self.bonds   = self.calculate_bond_lengths()

    def calculate_bond_lengths(self):

        all_dist = self.get_all_distances()

        bl = np.concatenate((np.diagonal(all_dist,1),
                             np.diagonal(all_dist,7)))

        assert len(bl) == 8, "Something wrong with this ring"


        return bl

    def view(self):
        ase.visualize.view(self)
        return

    def copy(self):
        """Return a copy."""
        return copy.deepcopy(self)


# main object that is a sulfur compound with S8 rings and other stuff
# input has to be an ase.atoms object
class SulfurCompound(ase.Atoms):

    def __init__(self,
                 structure=None,
                 positions=None, numbers=None,
                 tags=None, momenta=None, masses=None,
                 magmoms=None, charges=None,
                 scaled_positions=None,
                 cell=None, pbc=None, celldisp=None,
                 constraint=None,
                 calculator=None,
                 info=None,
                 velocities=None,
                 ):

        # initialize structure
        if not hasattr(structure, 'get_positions'):
            if isinstance(structure, str):
                structure = ase.io.read(structure)

        super().__init__(structure, positions, numbers,
        tags, momenta, masses, magmoms, charges, scaled_positions, cell,
        pbc, celldisp, constraint, calculator, info, velocities)


        self.rings_idx = self.label_sulfur_system()

        rings = []

        for idxs in self.rings_idx:
            rings.append(SulfurRing(structure, idxs))
        self.rings = rings


    # take a system with sulfur rings and label them (givin one id number to each ring)
    def label_sulfur_system(self):

        # take indexes of only the sulfur
        S_indexes = [atom.index for atom in self if atom.symbol == "S"]

        # initialize variables
        ring_list  = []
        used_index = []

        for index in S_indexes:

            if index in used_index: #index already used, ignore
                continue

            # find a new ring
            ring = self.find_sulphur_ring(index, ignore=used_index)

            # update ring list
            ring_list.append(ring)
            used_index.extend(ring) # add used indexes to the list

        return np.array(ring_list)

    # find sulphur ring given a single or multiple indexes of the ring
    def find_sulphur_ring(self, index, arrange=True, ignore=[]):

        S_indexes = np.where(np.array(self.get_chemical_symbols()) == "S")[0]

        ring_indexes = as_list(index)


        tmp_idx = ring_indexes[-1]
        while len(ring_indexes) < 8:

            next_idx = S_indexes[np.argsort(
                self.get_distances(tmp_idx, S_indexes, mic=True))]

            added_S = 0
            for ii in next_idx:

                if ii in ignore:
                    continue

                if ii not in ring_indexes:
                    ring_indexes.append(ii)
                    tmp_idx = ii
                    added_S += 1
                    if len(ring_indexes) == 8 or added_S >= 2:
                        break

                    continue

        if arrange:
            ring_indexes = self.arrange_ring_indexes(ring_indexes)

        return ring_indexes

    # arrange the indexes of a sulfur ring so that it is cyclic
    # neighbors are together
    # input:
        # system: system where the S ring is
        # ring_indexes: indexes of the ring
    def arrange_ring_indexes(self, ring_indexes):

        ri = ring_indexes.copy()
        ri.sort()

        reference_idx  = ri[0]
        arranged_idx = [reference_idx]
        idx_to_check = ri[1:]
        while len(idx_to_check) > 0:
            next_idx = idx_to_check[
                np.argsort(self.get_distances(
                    reference_idx, idx_to_check, mic=True))[0]
                ]
            arranged_idx.append(next_idx)
            reference_idx = next_idx
            idx_to_check.remove(next_idx)

        return arranged_idx

    def get_bonds_length(self, ring_indexes=None):

        if ring_indexes is None:
            rings = self.rings
        else:
            rings = [self.rings[ii] for ii in as_list(ring_indexes)]

        bl = []
        for ring in rings:
            bl.extend(ring.bonds)
        return bl

    def get_charge_on_rings(self, ring_indexes=None):

        if ring_indexes is None:
            rings = self.rings
        else:
            rings = [self.rings[ii] for ii in as_list(ring_indexes)]

        charges = []
        for ring in rings:
            charges.append(np.sum(ring.get_initial_charges()))
        return np.array(charges)

    def get_ring_label(self, index):

        ring_indexes = self.find_sulphur_ring(index)

        for cc, ring in enumerate(self.rings_idx):
            if set(ring_indexes) == set(ring):
                return cc

        raise "Error ring not found"

        return

    # calculate coordination number
    def get_coordination_number(self, atom1, atom2, d0, n=6, m=12):

        a1_idxs = atoms_to_indexes(self, atom1)
        a2_idxs = atoms_to_indexes(self, atom2)

        coordNum = []
        for idx in a1_idxs:
            dist = self.get_distances(idx, a2_idxs, mic=True)

            up = 1 - (dist/d0)**n
            dn = 1 - (dist/d0)**m
            coordNum.append(np.sum(up/dn))
        return coordNum[0] if len(coordNum) == 1 else coordNum

    # get coordinates between an atom and a sulfur ring in spherical coords
    def get_coordinates_from_ring(self, atom_index, ring_index, reference_S=None):

        def find_normal_to_ring(S_positions, COM):

            vectors = S_positions - COM

            vectors = np.append(vectors, [vectors[0]], axis=0)

            normals = []
            for cc, vec in enumerate(vectors[:-1]):
                normals.append(np.cross(vec, vectors[cc+1]))

            return np.sum(vectors, axis=0)

        def planeFit(points):
            """
            p, n = planeFit(points)

            Given an array, points, of shape (d,...)
            representing points in d-dimensional space,
            fit an d-dimensional plane to the points.
            Return a point, p, on the plane (the point-cloud centroid),
            and the normal, n.
            """
            points = np.reshape(points, (np.shape(points)[0], -1)) # Collapse trialing dimensions
            assert points.shape[0] <= points.shape[1], "There are only {} points in {} dimensions.".format(points.shape[1], points.shape[0])
            ctr = points.mean(axis=1)
            x = points - ctr[:,np.newaxis]
            M = np.dot(x, x.T) # Could also use np.cov(x) here.
            return ctr, np.linalg.svd(M)[0][:,-1]

        def project_on_plane(point, plane_point, plane_normal):
            return point - np.dot(point - plane_point, plane_normal) * plane_normal

        def angle_between(v1, v2):
            """ Returns the angle in radians between vectors 'v1' and 'v2'::
            """
            v1_u = v1/np.linalg.norm(v1)
            v2_u = v2/np.linalg.norm(v2)
            return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

        # get proper ring
        ring_indexes = self.rings_idx[ring_index].tolist()
        reference = self.__isolate_system(atom_index, ring_indexes)

        S_idx = [1,2,3,4,5,6,7,8]

        atom_pos = reference.get_positions()[0]
        S_pos = reference.get_positions()[1:]

        if reference_S is not None:
            closest_S = reference_S
        else:
            closest_S = [S_idx[ii] for ii in np.argsort(reference.get_distances(0, S_idx, mic=True))[:3]][0]


        # get com and normal to plane

        points_to_fit = np.append(S_pos, [np.mean(S_pos, axis=0)], axis=0)

        com, e1 = planeFit(points_to_fit.T)
        e1 = e1/np.linalg.norm(e1)

        # get e2
        pp = project_on_plane(reference.get_positions()[closest_S], com, e1)
        e2 = (pp - com)/np.linalg.norm(pp - com)

        if np.dot(e1, reference.get_positions()[closest_S] - pp) < 0:
            e1 = -e1

        # get e3
        e3 = np.cross(e1, e2)

        # find theta and phi and rad
        point_e2e3 = project_on_plane(atom_pos, com, e1)
        point_e1e2 = project_on_plane(atom_pos, com, e3)

        theta = angle_between(e1, atom_pos-com)#point_e1e2-com)
        phi   = angle_between(e2, point_e2e3-com)

        if np.dot(e3, atom_pos - point_e1e2) < 0:
            phi = -phi

        rad   = np.linalg.norm(atom_pos - com)

        return rad, 180*theta/np.pi, 180*phi/np.pi

    # apply distortion to a sulfur ring
    def apply_distortion_S8ring(self, distortion_site, write=False,
                                name="CONTCAR", opath=""):

        # check if ring indexes is not complete
        assert len(distortion_site) == 2, "Distortion site is made of two adjacent indexes."

        dist_idx = distortion_site.copy()

        assert any([dist_in_ring(distortion_site, ii) \
                    for ii in self.rings_idx]), "Distortion site does not belong to same ring"

        ring_indexes = self.find_sulphur_ring(dist_idx, arrange=False)
        ring_indexes = ring_indexes[1:] + [ring_indexes[0]]

        distortions = []
        for rindx in [ring_indexes, ring_indexes[::-1]]:
            reference = isolate_sulphur_ring(self, rindx)

            distortions.append(get_distortion_vector(reference))


        dist_vector = np.mean([distortions[0], distortions[1][::-1]], axis=0)

        # generate copy to distort system
        system_distorted = self.copy()
        system_distorted.positions[ring_indexes] = system_distorted.get_positions()[ring_indexes] + dist_vector

        # plot_systems(reference, arrows=dist_vec)

        # wrap em up
        system_distorted.wrap()

        # WRITE NICE LABELLED OUTPUT NOT SURE WHY THIS IS CAPS LOCK
        if write:

            ring_index = self.get_ring_label(ring_indexes)

            distortion_sites = generate_couples(self.rings_idx[ring_index])

            dist_site = np.where([set(dist_idx) == set(ii) for ii in distortion_sites])[0][0]

            ase.io.write(opath + name + "_pol_R{}D{}".format(str(ring_index).zfill(2),
                                                             str(dist_site).zfill(2)),
                         system_distorted, format="vasp", direct=True, vasp5=True,
                         ignore_constraints=True,
                         label="LiCS | ring: {} | dist: {}-{}".format(ring_index, *dist_idx))

        return system_distorted#, reference


    # isolate a system with only one ring and an extra ion, taking care of pbcs
    def __isolate_system(self, atom_index, ring_indexes):

        atom_label = self.get_chemical_symbols()[atom_index]


        # total index
        tot_idx = ring_indexes.copy()
        tot_idx.insert(0, atom_index)

        new_pos = self.get_positions()[tot_idx]

        # generate reference ring from system and indexes
        reference = ase.Atoms(symbols   = atom_label + "S8",
                              positions = new_pos,
                              cell      = self.get_cell(),
                              pbc       = self.get_pbc
                              )

        vec = reference.get_all_distances(mic=True, vector=True)[1]
        new_cell = np.array([100, 100, 100])
        vec += new_cell/2

        reference.set_cell(np.diag(new_cell))
        reference.set_positions(vec)
        # take care of pbc issues

        return reference

    def view(self):
        ase.visualize.view(self)
        return

    # def copy(self):
    #     """Return a copy."""
    #     atoms = self.__class__(cell=self.cell, pbc=self.pbc, info=self.info,
    #                            celldisp=self._celldisp.copy())

    #     atoms.arrays = {}
    #     for name, a in self.arrays.items():
    #         atoms.arrays[name] = a.copy()
    #     atoms.constraints = copy.deepcopy(self.constraints)
    #     return atoms

    def copy(self):
        """Return a copy."""
        return copy.deepcopy(self)