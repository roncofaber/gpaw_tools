#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 17:56:45 2022

@author: roncoroni
"""

# Operations
import numpy as np

# ASE
import ase
from ase.units import Bohr

# GPAW
import gpaw
from gpaw.cdft.cdft import WeightFunc

# mixed stuff
import pickle
import os
import warnings

# plotting
import matplotlib.pyplot as plt
import matplotlib.colors as matcol
import seaborn as sns
sns.set_style("white")

#%%

# check if list of strings or not
def check_list_of_str(obj):
    return bool(obj) and all(isinstance(elem, str) for elem in obj)

def as_list(inp):
    return [inp] if not isinstance(inp, list) else inp

#%%
# Generate DOS_object from a .gpw file and store it as a pickle object to use
# when to plot the DOS.
class DOS_object():

    def __init__(self,
                 filein,               # name of input file, either gpaw file or pkl
                 save     = True,      # save or not as pickle
                 savename = "default", # name of the pickle object
                 verbose  = True      # print stuff when creating
                 ):

        # check that it's a string
        assert isinstance(filein, str)

        # check if it's a new wfs file or a already processed pkl file
        if filein.endswith(".gpw"):

            # read data
            self.__initialize_data(filein, save, savename, verbose)

        # restart from pickle file
        elif filein.endswith(".pkl"):
            self.__restart_dos_from_pickle(filein)

        return

    # read system and store all relevant data
    def __initialize_data(self, filein, save, savename, verbose):

        self.filein = filein

        if verbose:
            print("Starting to read .gpw file: {}".format(filein))

        system, __ = gpaw.restart(filein)

        # get other relevant infos
        self.chem_symbols  = system.get_chemical_symbols()
        self.atoms         = system.calc.atoms # get from here cause less memory?

        if verbose:
            print("Loaded gpaw file, found following atom system:")
            print("{}".format(system.get_chemical_formula()))

        # get wavefunctions
        wfs = system.calc.wfs

        if verbose:
            print("Nice wavefunctions you got there! - time to get the DOS")

        try:
            efermi = system.calc.get_fermi_level()

            if verbose:
                print("Found E_fermi: {}".format(efermi))

            self.efermi = [efermi, efermi]
        except:
            efermi = system.calc.get_fermi_levels()

            if verbose:
                print("Found two E_fermis: {} | {}".format(*efermi))

            self.efermi = efermi

        # calculate number of bands
        nbands = len(system.calc.get_eigenvalues())

        # calculate number of projections
        nproj = 0
        proj_idx = []
        labels   = []
        for cc, setup in enumerate(wfs.setups):

            nproj += setup.ni
            proj_idx.extend(setup.ni*[cc])
            labels.extend(self.__generate_label(setup))

        # calculate number of spins
        nspins = wfs.nspins

        # calculate number of kpts
        nkpts = int(len(wfs.kpt_u)/nspins)

        if verbose:
            print("Initializing weight matrix, hang in there", end="")

        # initialize data matrix
        W_sknp = np.zeros((nspins, nkpts, nbands, nproj))
        eps = np.zeros((nspins, nkpts, nbands))
        occ = np.zeros((nspins, nkpts, nbands))
        kpt_weights = []

        # loop over spin and kpt to get data
        for kpt, wgt in enumerate(wfs.kd.weight_k):
            for spin in range(nspins):
                W_sknp[spin, kpt, :, :] = wgt*abs(wfs.collect_projections(kpt,
                                                                          spin))**2
                eps[spin, kpt, :] = system.calc.get_eigenvalues(kpt=kpt,
                                                                spin=spin)
                occ[spin, kpt, :] = system.calc.get_occupation_numbers(kpt=kpt,
                                                                       spin=spin)
                if verbose: print(".", end="", flush=True)
            kpt_weights.append(wgt)

        if verbose:
            print(" done.")
            print("Data matrix completed - finalizing DOS object")

        # get relevant energy range
        Emin = np.min(eps)
        Emax = np.max(eps)
        dE   = Emax - Emin
        self.Emin = Emin - 0.05*dE
        self.Emax = Emax + 0.05*dE

        if verbose:
            print("Calculated energy interval.\n"\
                  " Emin: {}, Emax: {}".format(self.Emin,self.Emax))

        # store data
        self.W_sknp      = W_sknp
        self.kpt_weights = np.array(kpt_weights)
        self.eps         = eps
        self.occ         = occ
        self.proj_idx    = np.array(proj_idx)
        self.labels      = labels

        self.nspins = nspins
        self.nkpts  = nkpts
        self.nbands = nbands
        self.nproj  = nproj

        self.is_cdft = False
        # check if it was a CDFT calculation
        ext_pot = system.calc.parameters["external"]
        if ext_pot is not None and ext_pot.get('name') is not None:
            if ext_pot.get("name") == "CDFTPotential":
                self.is_cdft = True
                self.cdft_regions = ext_pot.get("regions")
                self.cdft_constr  = ext_pot.get('constraints')

                if verbose: print("This was a CDFT calculation,"\
                                  " you rascal. Don't worry I got it.")

        if self.is_cdft:
            cdft_contr, weight_functions = self.__calculate_CDFT_contribution(system)
            self.cdft_contr = cdft_contr
            self.weight_functions = weight_functions

        # remove system to be sure
        del system

        self.faberwgt_check = True

        # save file if requested
        if save:
            self.__save_self(oname=savename)

        return

    # Used to calculate the contribution of the CDFT constraints
    # to the orbital energies - runs automatically if system used CDFT.
    def __calculate_CDFT_contribution(self, system):

        print("Starting to calculate CDFT contribution - might take long...")

        # get coarse grid
        gd = system.calc.density.gd

        # initialize contrained contribution
        cdft_contr = np.zeros((self.nspins, self.nkpts, self.nbands))

        weight_functions = []
        # enumerate over all constrained region
        for cc, cdft_region in enumerate(self.cdft_regions):

            print("Getting weight function for the {} cdft region.".format(cc))
            # calculate weight function
            W = WeightFunc(gd, self.atoms, cdft_region)
            wgtfun = W.construct_weight_function()

            # iterate over spin, kpt, bands
            for ss in range(self.nspins):
                for kk in range(self.nkpts):
                    print("Calculating orbital dependent contribution for:")
                    print("spin: {}, kpt: {}".format(ss, kk))
                    for nn in range(self.nbands):

                        # get pseud0 WF TODO
                        wf = system.calc.get_pseudo_wave_function(
                                                                  band = nn,
                                                                  kpt  = kk,
                                                                  spin = ss
                                                               )
                        # calculate density
                        dens = np.real(np.conj(wf)*wf)

                        # calculate contribution
                        contr = self.kpt_weights[kk]*self.cdft_constr[cc]*\
                            gd.integrate(wgtfun, dens)*Bohr**3

                        # save contribution
                        cdft_contr[ss,kk,nn] += contr

                        print("\r{:5.2f}% complete".format(100*(nn+1)/self.nbands),
                              end="", flush=True)
                    print("")
            weight_functions.append(wgtfun)


        return cdft_contr, weight_functions

    # restart object from pkl file previously saved
    def __restart_dos_from_pickle(self, filein):

        # open previously generated gpw file
        with open(filein, "rb") as fin:
            restart = pickle.load(fin)

        self.__dict__ = restart.__dict__.copy()

        assert hasattr(self, "faberwgt_check"), "Cannot load non compatible object!"

        # efermi check, compatible with older stuff
        if isinstance(self.efermi, np.floating):
            self.efermi = [self.efermi, self.efermi]

        return

    # save object as pkl file
    def __save_self(self, oname):

        path = os.getcwd()

        # check output name
        if oname == "default":
            oname = path + "/dos_object_wgt.pkl"
        else:
            if oname.endswith(".pkl"):
                oname =  path + "/" + oname
            else:
                oname = path + "/" + oname.split(".")[-1] + ".pkl"

        with open(oname, 'wb') as fout:
            pickle.dump(self, fout)

        print("Saved everything as {}".format(oname))
        return

    # assign labels depending on l, m quantum numbers
    def __generate_label(self, setup):

        n_qns = setup.n_j
        l_qns = setup.l_j

        labels = []
        for cc in range(len(n_qns)):

            l_qn = l_qns[cc]
            for mm in range(2*l_qn + 1):

                label = {}
                label["n"] = n_qns[cc]
                label["l"] = ["s", "p", "d", "f"][l_qn]
                label["m"] = [[""], ["y", "z", "x"],
                              ["xy", "yz", "z2", "zx", "x2-y2"],
                              ["f1", "f2", "f3", "f4", "f5", "f6", "f7"]][l_qn][mm]
                labels.append(label)

        return np.array(labels)

    # return a list of int to label QNs into a label that can be used
    def num_to_str_QN(self, n_qn=None, l_qn=None, m_qn=None):

        if all([ii is None for ii in [n_qn, l_qn, m_qn]]):
            return False

        label = {}

        if n_qn is not None:
            label["n"] = as_list(n_qn)

        if l_qn is None:
            return label

        label["l"] = [["s", "p", "d", "f"][ii] for ii in as_list(l_qn)]

        if m_qn is None or isinstance(l_qn, list):
            return label

        label["m"] = [[[""], ["y", "z", "x"],
                      ["xy", "yz", "z2", "zx", "x2-y2"],
                      ["f1", "f2", "f3", "f4", "f5", "f6", "f7"]
                      ][l_qn][ii] for ii in m_qn]
        return label

    def __gaussian(self, x, x0, width):
        return np.exp(-((x0 - x) / width)**2) / (np.sqrt(np.pi) * width)

    # convert list of symbols or list of indexes to just a list of indexes
    def __atoms_to_indexes(self, symbols):


        # check if symbols is a list of strings
        if symbols == "all":
            return list(range(len(self.chem_symbols)))

        if not isinstance(symbols, list):
            symbols = [symbols]

        indexes = []
        for symbol in symbols:
            if not isinstance(symbol, str):
                indexes.append(symbol)
            else:
                for cc, atom in enumerate(self.chem_symbols):
                    if atom == symbol:
                        indexes.append(cc)

        return indexes

    # split indexes of projectors so that the code can shift them for cdft
    def __split_indexes_for_CDFT(self, indexes):

        split_indexes = [ [] for _ in range(len(self.cdft_constr)+1) ]

        Vc_shifts = [0]
        Vc_shifts.extend( [ii for ii in self.cdft_constr])

        for index in indexes:
            atom = self.proj_idx[index]

            found = False
            for cc, cdft_region in enumerate(self.cdft_regions):
                if atom in cdft_region:
                    split_indexes[cc+1].append(index)
                    found = True
                    break
            if not found:
                split_indexes[0].append(index)

        # filter empty stuff
        Vc_shifts = [ii for cc, ii in enumerate(Vc_shifts) if split_indexes[cc]]
        split_indexes = [ii for ii in split_indexes if ii]
        return split_indexes, Vc_shifts

    def get_atomic_weight_on_band(self, spin, kpt, band, atoms):

        # get indexes of atoms
        a_idxs = self.__atoms_to_indexes(atoms)

        # get projector indexes
        p_idxs = np.where([ii in a_idxs for ii in self.proj_idx])[0].tolist()

        return np.sum(self.W_sknp[spin, kpt, band, p_idxs])

    def get_atomic_weights_on_band(self, spin, kpt, band):

        atoms_list = list(set(self.chem_symbols))

        weights = {}
        for atom in atoms_list:

            weights[atom] = self.get_atomic_weight_on_band(spin, kpt, band, atom)

        return weights

    def get_orbital_weights(self, spin, kpt, atoms):

        # get indexes of atoms
        a_idxs = self.__atoms_to_indexes(atoms)

        # get projector indexes
        p_idxs = np.where([ii in a_idxs for ii in self.proj_idx])[0].tolist()

        return np.sum(self.W_sknp[spin, kpt, :, p_idxs], axis=0)

    def print_atomic_weights_on_band(self, spin, kpt, band):

        weights = self.get_atomic_weights_on_band(spin, kpt, band)

        tot_wgt = np.sum([weights[ii] for ii in weights])

        print(24*"_")
        print("Atomic contributions on:")
        print("b : {:4}, k: {:2}, s: {:1}".format(band, kpt, spin))
        print(15*"-")
        for cc, atom in enumerate(weights):
            print("{:<4} : {:>5.1f}%".format(atom, 100*weights[cc]/tot_wgt))
        print(24*"_")
        return

    def print_weight_function_as_cube(self, cdft_region=None):

        if cdft_region is None:
            region_to_print = range(len(self.cdft_regions))
        else:
            region_to_print = as_list(cdft_region)


        for ii in region_to_print:
            with open("weight_function_{}.cube".format(str(ii).zfill(3)), "w") as fout:
                ase.io.cube.write_cube(fout, self.atoms,
                                       data=self.weight_functions[ii],
                                       origin=None, comment=None)

        return

    def get_fractional_weight_on_bands(self, atoms, spin=0, kpt=0):

        ato_W = []
        all_W = []


        ato_W = self.get_orbital_weights(spin, kpt, atoms)
        all_W = self.get_orbital_weights(spin, kpt, "all")

        energies = self.eps[spin,kpt,:]

        return energies, ato_W/all_W

    # broaded DOS data from data matrix
    def broaden_DOS(self, energies, weights, sigma, npoints):

        # broaden DOS
        Emin = self.Emin
        Emax = self.Emax

        e_range = np.linspace(Emin, Emax, npoints)
        dos = np.zeros((self.nspins, len(e_range)))

        for e, w in zip(energies.T, weights.T):

            for spin in range(self.nspins):

                dos[spin] += w[spin]*self.__gaussian(e[spin], e_range, sigma)

        return e_range, dos


    # given atoms and qns, generate projector indexes
    def generate_proj_idxs(self,
                           atoms,
                           n_qn   = None,
                           l_qn   = None,
                           m_qn   = None,
                           ):

        # get indexes of atoms
        a_idxs = self.__atoms_to_indexes(atoms)

        # get projector indexes
        p_idxs = np.where([ii in a_idxs for ii in self.proj_idx])[0].tolist()

        # generate main label to check
        main_label = self.num_to_str_QN(n_qn, l_qn, m_qn)

        if not main_label: # take all of them anyway...
            return p_idxs

        indexes = []

        # iterate all indexes and check if it makes sense
        for pidx in p_idxs:

            # get pidx label
            label = self.labels[pidx]

            if all([label[qn] in main_label[qn] for qn in main_label]):
                indexes.append(pidx)

        return indexes

    # main function to obtain the PDOS
    def get_pdos(self,
                atoms,
                n_qn       = None,
                l_qn       = None,
                m_qn       = None,
                npoints    = 3001,
                sigma      = 0.01,
                cdft_shift = False,
                ):

        # generate proj_indexes to read
        indexes = self.generate_proj_idxs(atoms,
                                          n_qn,
                                          l_qn,
                                          m_qn
                                          )

        # # check if possible to shift states
        if cdft_shift and not self.is_cdft:
            warnings.warn("This is not a CDFT calculation - ignoring"\
                          " the cdft_shift keyword")
            cdft_shift = False

        # Read weight matrix and calculate DOS
        energies    = [[], []]
        weights_tot = [[], []]
        weights_occ = [[], []]

        # get weight for selected indexes
        wgt = np.sum(self.W_sknp[:,:,:,indexes], axis=3)

        # iterate over kpoints, spin is automatically done
        for kpt in range(self.nkpts):

            # get occupation
            occ = self.occ[:,kpt,:]/self.kpt_weights[kpt]

            # get eps
            if not cdft_shift:
                e = self.eps[:,kpt,:]
            else:
                e = self.eps[:,kpt,:] - self.cdft_contr[:,kpt,:]

            # get weight
            w = wgt[:,kpt,:]

            # append data
            energies    = np.concatenate((energies,     e), axis=1)
            weights_tot = np.concatenate((weights_tot,  w), axis=1)
            weights_occ = np.concatenate((weights_occ, occ*w), axis=1)

        # broaden DOS
        e_range, dos_tot = self.broaden_DOS(energies, weights_tot, sigma, npoints)
        __,      dos_occ = self.broaden_DOS(energies, weights_occ, sigma, npoints)

        return e_range, dos_tot, dos_occ

# END OF CLASS

#%% OTHER FUNCTIONS

# add a dos line to a ax object
def add_dos_line(ax, energy, dos_tot, dos_occ, efermi, label, color, lw=1.5,
                 occ_to_ef=True, shift_ef=False):

    s = [1,-1]

    if shift_ef:
        E  = energy - efermi[0]
        Ef = [0, efermi[1] - efermi[0]]
    else:
        E  = energy
        Ef = efermi

    for spin in [0,1]:

        # fill dos
        if occ_to_ef:
            ax.fill_between(E,  s[spin]*dos_tot[spin], 0,
                            where=(E <= Ef[spin]),
                            color=color,
                            linewidth=lw,
                            label=label if not spin else '_nolegend_',
                            zorder=0)
        else:
            ax.fill_between(E,  s[spin]*dos_occ[spin], 0,
                            where=(E <= Ef[spin]),
                            color=color,
                            linewidth=lw,
                            label=label if not spin else '_nolegend_',
                            zorder=0)

        # add black lines
        idx = np.where(E<=Ef[spin])
        ax.plot(E[idx],  s[spin]*dos_tot[spin][idx], color="k", linewidth=lw, zorder=1000)

        idx = np.where(E>=Ef[spin]-0.01)
        ax.plot(E[idx],  s[spin]*dos_tot[spin][idx], color=color, linewidth=lw, zorder=100)

    return

def generate_DOS_plot(dos,
                      dos_elements,
                      cdft_shift = False,
                      npoints    = 5001,
                      plot_ef    = True,
                      shift_ef   = False,
                      xlim       = None,
                      ylim       = None,
                      occ_to_ef  = True,
                      fd_width   = 0.01,
                      title      = None,
                      ):

    lw = 1.5
    fs = 13
    fig, ax = plt.subplots(figsize=(8,6))

    cmap = sns.color_palette(n_colors=len(dos_elements))
    used_color = 0
    for cc, ele in enumerate(dos_elements):

        # check label quality
        assert "a" in ele.keys(), "Please specify atoms with key 'a'."
        atoms = ele["a"]

        if isinstance(atoms, np.ndarray):
            atoms = atoms.tolist()

        if "c" in ele.keys():
            assert matcol.is_color_like(ele["c"]), "Wrong color specified {}"\
                .format(ele["c"])
            col = ele["c"]
        else:
            col = matcol.to_hex(cmap[used_color])
            used_color += 1

        if "l" in ele.keys():
            lab = ele["l"]
        else:
            lab = "_nolegend_"

        if "qn" in ele.keys():
            assert len(ele["qn"]) == 3, "Please specify correct QN."
            n_qn, l_qn, m_qn = ele["qn"]
        else:
            n_qn, l_qn, m_qn = None, None, None

        e, d, do = dos.get_pdos(atoms,
                                cdft_shift = cdft_shift,
                                npoints    = npoints,
                                n_qn       = n_qn,
                                l_qn       = l_qn,
                                m_qn       = m_qn,
                                sigma      = fd_width
                                )

        add_dos_line(ax, e, d, do, dos.efermi, lab, col,
                     lw=lw, occ_to_ef=occ_to_ef, shift_ef=shift_ef)


    if plot_ef:
        for spin in [0,1]:
            ymn = [0.5, 0.0]
            ymx = [1.0, 0.5]

            if shift_ef:
                Ef = dos.efermi[spin] - dos.efermi[0]
            else:
                Ef = dos.efermi[spin]

            ax.axvline(x=Ef, linestyle="--", color="r",
                       linewidth=lw, ymin=ymn[spin], ymax=ymx[spin],
                       zorder=9999)

    if ylim is not None:
        ax.set_ylim(ylim)

    if xlim is not None:
        ax.set_xlim(xlim)

    if shift_ef:
        ax.set_xlabel("Energy - E_fermi [eV]", fontsize=fs)
    else:
        ax.set_xlabel("Energy [eV]", fontsize=fs)

    ax.set_ylabel("PDOS [-]", fontsize=fs)

    if title is None:
        ax.set_title("/".join(dos.filein.split("/")[-3:]))
    else:
        ax.set_title(title)

    ax.legend(loc="upper right").set_zorder(99999)


    fig.tight_layout()
    fig.show()


    return fig

def plot_orbitals_as_sticks(dos, atoms, kpt=0, ef_region=False):

    if isinstance(atoms, np.ndarray):
        atoms = atoms.tolist()

    fig, ax = plt.subplots(figsize=(8,6))

    # fig.tight_layout()

    wmaxs = []
    for spin in [0,1]:
        energies, wgts = dos.get_fractional_weight_on_bands(atoms, spin=spin, kpt=kpt)
        # __,       wgts_tot = dos.get_fractional_weight_on_bands("all", spin=spin, kpt=kpt)

        s = [1, -1][spin]
        wmax = np.max(wgts)
        wmaxs.append(wmax)


        ax.vlines(energies, ymin=np.minimum(0,s*wgts),
                  ymax=np.maximum(0,s*wgts),
                   color="red", lw=1, zorder=99999)

        ax.vlines(energies, ymin=np.min((0,s*wmax)), ymax=np.max((0,s*wmax)),
                   color="k", lw=1.0)

        for cc, ee in enumerate(energies):
            if cc%5 == 0:
                ax.text(ee, s*wmax, str(cc), rotation="vertical",
                        ha='center', va='center')

    if ef_region:
        min_ef = np.min(dos.efermi)
        max_ef = np.max(dos.efermi)
        ax.set_xlim([min_ef-3, max_ef+3])


    ax.axhline(y=0, color="k", lw=1.5)

    for spin in [0,1]:
        ymn = [0.5, 0.0]
        ymx = [1.0, 0.5]
        ax.axvline(x=dos.efermi[spin], linestyle="--", color="orange",
                   linewidth=1.2, ymin=ymn[spin], ymax=ymx[spin])

    ax.set_ylim([-np.max(wmaxs),np.max(wmaxs)])


    fig.show()

    return