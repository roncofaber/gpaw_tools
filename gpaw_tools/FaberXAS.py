#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 09:19:12 2022

@author: roncofaber
"""

# GPAW
import gpaw
from gpaw_tools.XAS_faber import XAS
import matplotlib.pyplot as plt

# other functions
import numpy as np
import glob
import os
import pickle

#%%

# dummy xasObj class to use to save XAS in bigger object
class xasObj():
    def __init__(self):
        return

class XAS_spectra():

    def __init__(
            self,
            basepath = ".",
            setpath  = ".",
            wfs_name = None,
            logfile  = "LOG",
            multiple = False,
            oname    = "default",
            save     = True,
            restart  = False,
            xch_spin = None,
            ):

        if not restart:
            self.__initialize_from_wavefunctions(basepath, setpath, wfs_name, logfile, multiple, oname, save,
                                                 xch_spin)
        else:
            self.__restart_XAS_from_pickle(basepath, restart)

        return

    # run XAS on new wfs file
    def __initialize_from_wavefunctions(self,
                                        basepath,
                                        setpath,
                                        wfs_name,
                                        logfile,
                                        multiple,
                                        oname,
                                        save,
                                        xch_spin
                                        ):
        # set attributes
        self.basepath = basepath
        self.setpath  = setpath
        self.wfs_name = wfs_name
        self.logfile  = logfile

        self.xch_spin = xch_spin

        # add setup path
        gpaw.setup_paths.insert(0, setpath)

        if not multiple:
            self.subpaths = [basepath]
        else:
            self.subpaths = self.__find_gpw_subdirectories()

        # generate list with all XAS
        self.xas_list = self.__generate_XAS_list()

        # save object
        if save:
            self.__save_self(oname)

        return

    # restart XAS object from a pkl file
    def __restart_XAS_from_pickle(self, basepath, restart):

        # open previously generated gpw file
        with open(basepath + restart, "rb") as fin:
            restart = pickle.load(fin)

        self.__dict__ = restart.__dict__.copy()

        return

    # find all subdirectories with a gpw file and return as list
    def __find_gpw_subdirectories(self):

        gpw_files = glob.glob(self.basepath + "*/*.gpw", recursive=True)
        gpw_files.sort()

        subpaths = []

        for gpw_file in gpw_files:
            subpaths.append(os.path.dirname(gpw_file) + "/")

        return subpaths


    # generate a list of all XAS objects (one per excited atom)
    def __generate_XAS_list(self):

        xas_list = []
        for subpath in self.subpaths:
            xas = self.__read_single_XAS(subpath, self.wfs_name,
                                         self.logfile, self.xch_spin)
            xas_list.append(xas)

        return xas_list

    # read a single XAS folder
    def __read_single_XAS(self, path, wfs_name, logfile, xch_spin=None):

        # read wavefunctions
        print("Reading wavefunctions from: {}".format(path+wfs_name))
        calc = gpaw.GPAW(path + wfs_name)

        # read delta K-S energy
        with open(path + logfile, "r") as fin:
            for line in fin:
                if line.startswith("dks:"):
                    dks = float(line.split()[1])
                    break

        # crete a xas object
        xas = xasObj()

        # assign infos
        xas.dks = dks
        xas.calc_details = calc.todict()
        # if spinpol
        if xas.calc_details["spinpol"]:

            if calc.get_magnetic_moment() >= 0:
                spin_0 = 0
                spin_1 = 1
            else:
                spin_0 = 1
                spin_1 = 0

            # define xas.up as the xas of the channel with the magnetic moment
            xas.up = XAS(calc, mode='xas', spin=spin_0, xch_spin=xch_spin) #TODO
            xas.dn = XAS(calc, mode='xas', spin=spin_1, xch_spin=xch_spin)
        else:
            xas.up = XAS(calc, mode='xas', xch_spin=xch_spin)

        del calc

        return xas

    # return values of index idnr of the XAS spectra
    def get_single_XAS_spectra(self,
                               xas,
                               spin       = "up", #default is spin up
                               e_gs       = 0,
                               projection = None,
                               ):

        # check projection, if not specified do carthesia axis
        if projection is None:
            projection = [
                [1,0,0],
                [0,1,0],
                [0,0,1]
                ]

        # get peak positions and intensities
        x_s, y_s = getattr(xas, spin).get_spectra(
                                                stick    = True,
                                                proj     = projection,
                                                proj_xyz = False,)

        # calculate energy shift because of dks
        dks_energy = xas.dks - e_gs
        x_shift = dks_energy - x_s[0]

        # return shifted plot
        return x_s + x_shift, y_s

    # get full xas spectra with all contributions
    def get_full_XAS_spectra(self,
                             atoms   = "all",
                             spin    = "up",
                             npoints = 2001,
                             weights = None,
                             sigma   = 0.5,
                             e_gs    = 0,
                             theta   = None, # angle on the z axis (0-pi)
                             phi     = None  # angle on the xy plane (0-2pi)
                             ):

        # select XAS objects to plot (e.g. only contribution of some excitations)
        if atoms == "all":
            xas_list = self.xas_list
        elif type(atoms) is list: #is list
            xas_list = [self.xas_list[ii] for ii in atoms]
        elif type(atoms) is int:
            xas_list = [self.xas_list[atoms]]
        else:
            raise ValueError('Check your "atoms" variable please...')

        # check that atoms and weights have the same length
        if weights is not None:
            if type(weights ) is int:
                weights = [weights]
            assert len(xas_list) == len(weights), "Weights and atoms have different length!"
        else:
            weights = len(xas_list)*[1]

        # Calculate E-field vector for the projection
        # angles are specified
        if theta is not None and phi is not None:
            projection = [
                np.cos(phi)*np.sin(theta),
                np.sin(phi)*np.sin(theta),
                np.cos(theta)
                ]
            nproj = 1
        # angles are not specified
        else:
            projection = [
                [1,0,0],
                [0,1,0],
                [0,0,1]
                ]
            nproj = len(projection)

        x_s_full = []
        y_s_full = np.array([[] for ii in range(nproj)])

        # iterate over all atoms to get contributions as Ix, Iy, Iz
        for cc, xas in enumerate(xas_list):

            x_s, y_s = self.get_single_XAS_spectra(xas,
                                                   spin       = spin,
                                                   e_gs       = e_gs,
                                                   projection = projection
                                                   )

            # add energies and intensities in the big array soup
            x_s_full.extend(x_s)
            y_s_full = np.concatenate((y_s_full, weights[cc]*y_s), axis=1)

        # define energy range
        Emin = np.min(x_s_full)
        Emax = np.max(x_s_full)
        dE   = np.abs(Emax - Emin)
        E_range = np.linspace(Emin-0.05*dE, Emax+0.05*dE, npoints)

        # calculate broad spectra
        broad_spectra = self.broaden_spectrum(x_s_full, y_s_full, E_range, sigma)

        # return randomly oriented e-field along xy plane
        if theta is not None and phi is None:
            Ix, Iy, Iz = broad_spectra
            broad_spectra = 0.5*(Ix + Iy)*np.sin(theta)**2 + Iz*np.cos(theta)**2

        elif theta is None and phi is None:
            Ix, Iy, Iz = broad_spectra
            broad_spectra = (Ix + Iy + Iz)/3
        else:
            broad_spectra = broad_spectra[0]


        return E_range, np.array(x_s_full), y_s_full, broad_spectra

    def get_XPS_energies(self, e_gs=None):

        xps_energies = np.array([ii.dks for ii in self.xas_list])

        if e_gs is not None:
            return xps_energies - e_gs
        else:
            return xps_energies



    # broaden spectrum to plot it
    def broaden_spectrum(self, x_peaks, y_peaks, energies, sigma=0.5):

        gaussian_spectrum = np.zeros((np.shape(y_peaks)[0], len(energies)))
        energies = np.reshape(energies, (1, len(energies)))

        for x_p, y_p in zip(x_peaks, y_peaks.T):

            y_p = y_p.reshape(np.shape(y_p)[0], -1)

            gaussian_spectrum += y_p*np.exp(-((((x_p - energies)/sigma)**2)))

        return gaussian_spectrum

    # save spectra as pkl object
    def __save_self(self, oname):

        # check output name
        if oname == "default":
            oname = "XAS_object.pkl"
        elif not oname.endswith(".pkl"):
            oname = oname.split(".")[0] + ".pkl"


        with open(self.basepath + oname, 'wb') as fout:
            pickle.dump(self, fout)

        print("Saved everything as {}".format(oname))
        return