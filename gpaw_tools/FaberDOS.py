#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 13:17:54 2021

@author: roncofaber
"""

import gpaw
import gpaw.dos as gdos
import numpy as np
import pickle
# %% FUNCTIONS

# check if list of strings or not
def check_list_of_str(obj):
        return bool(obj) and all(isinstance(elem, str) for elem in obj)

# dummy object that makes a dos entity
class dos_entity():

    def __init__(self):
        return

#%% MAIN CLASS
class myDOS():

    def __init__(self,
                 gpw_file,
                 npoints  = 2001,      # number of points in dos
                 spinpol  = True,      # TODO, for the moment only spinpol calc are allowed
                 save     = True,      # save or not as pickle
                 savename = "default", # name of the pickle object
                 width    = 0.1,       # width of the DOS, if 0 thetra method
                 wip      = False      # use True to use the WIP version of the pdos calc
                 ):

        # check if it's a new wfs file or a already processed pkl file
        if gpw_file.endswith(".gpw"):
            self.__initialize_dos(gpw_file, npoints, save, savename, width, wip)
        elif gpw_file.endswith(".pkl"):
            self.__restart_dos_from_pickle(gpw_file)


        return

    # read a new gpw restart file and process the DOS
    def __initialize_dos(self, gpw_file, npoints, save, savename, width, wip):

        # save gpw file path (useful for later)
        self.gpw_file = gpw_file

        # set width for dos
        self.width = width

        # set number of points in DOS
        self.npoints = npoints

        if width == 0:
            print("-----------------------------------------")
            print("WARNING: you choose width = 0 which means")
            print("tetrahedron integration will be used and ")
            print("is very slow. Chill and wait in the meantime.")
            print("-----------------------------------------")

        print("Starting to read .gpw file: {}".format(gpw_file))
        
        # Run in legacy mode (works perfectly fine but not sure of the results...)
        if not wip:
            # read calculator and generate dos object
            gpaw_calculator = gpaw.GPAW(gpw_file)
            self.atoms = gpaw_calculator.atoms.copy()
            print("Read gpaw file, found atom system:")
            print("{}".format(self.atoms.get_chemical_formula()))
            
            print("File loaded - time to get the DOS")
    
            # get efermi
            try:
                efermi = gpaw_calculator.get_fermi_level()
                print("Found E_fermi: {}".format(efermi))
                self.n_efermi = [0]
    
            except:
                efermi = gpaw_calculator.get_fermi_levels()
                print("Found two E_fermis: {} | {}".format(*efermi))
                self.n_efermi = [0,1]
    
            self.efermi = efermi
    
    
    
            # get total dos
            self.energies, self.tot_dos = self.__calculate_total_dos(gpaw_calculator)
            
    
            for ii in self.n_efermi:
    
                print("Calculated energy interval. Emin: {}, Emax: {}".format(np.min(self.energies[ii]),
                                                                              np.max(self.energies[ii])))
            
            print("Got total DOS from .gpw file.")
            
            # get all pdos
            print("Starting to calculate the PDOS for every atom. Hang in there.")
            self.system = self.__calculate_pdos(gpaw_calculator)
            
            # remove them after initialization cause they
            # use too much memory
            del gpaw_calculator 

        # Use DOScalculator to get the PDOS, let's see if it works
        else:
            
            # TODO THIS IS NOT CLEVER AS DOSCalculator already load the gpw file
            
            # Read first gpaw calculator to get atoms
            gpaw_calculator = gpaw.GPAW(gpw_file)
            
            self.atoms = gpaw_calculator.atoms.copy()
            print("Read gpaw file, found atom system:")
            print("{}".format(self.atoms.get_chemical_formula()))
            
            del gpaw_calculator
            
            dos_calculator = gdos.DOSCalculator.from_calculator(gpw_file, shift_fermi_level=False)
            print("File loaded and generated DOSCalculator object - time to get the DOS")
            
            # get efermi
            efermi = dos_calculator.fermi_level
            
            print("Found E_fermi: {}".format(efermi))
            self.n_efermi = [0]
            self.efermi = efermi
            
            # get energies
            self.energies = self.__get_energies_wip(dos_calculator)
            
            for ii in self.n_efermi:
                print("Calculated energy interval. Emin: {}, Emax: {}".format(np.min(self.energies[ii]),
                                                                          np.max(self.energies[ii])))
            # get total dos
            self.tot_dos = self.__calculate_total_dos_wip(dos_calculator)
            print("Got total DOS from .gpw file.")

            # get all pdos
            print("Starting to calculate the PDOS for every atom. Hang in there.")
            self.system = self.__calculate_pdos_wip(dos_calculator)
            
            # remove them after initialization cause they
            # use too much memory
            del dos_calculator 
        
        # save file if requested
        if save:
            self.__save_self(oname=savename)

        return

    def __restart_dos_from_pickle(self, gpw_file):

        # open previously generated gpw file
        with open(gpw_file, "rb") as fin:
            restart = pickle.load(fin)

        self.__dict__ = restart.__dict__.copy()

        return
    
    # Return array with energies
    def __get_energies_wip(self, dos_calculator):
        
        emin, emax = dos_calculator.get_energies(npoints=2)    
        dE = np.abs(emax - emin)
        emin -= 0.1*dE
        emax += 0.1*dE
        
        energies = []
        for spin in [0,1]:
            energies.append(np.linspace(emin, emax, self.npoints))
            
        return np.array(energies)
    
    # Get total DOS using the WIP feature
    def __calculate_total_dos_wip(self, dos_calculator):
        
        tot_dos = []
        for spin in [0, 1]:
            tot_dos.append(dos_calculator.raw_dos(energies = self.energies[spin],
                                                  spin     = spin,
                                                  width    = self.width))
        
        return np.array(tot_dos)

    # get total dos
    def __calculate_total_dos(self, gpaw_calculator):

        energies = []
        tot_dos  = []
        for spin in [0, 1]:
            E, dos = gpaw_calculator.get_dos(spin=spin, npts=self.npoints, width=self.width)

            energies.append(E)
            tot_dos.append(dos)

        return np.array(energies), np.array(tot_dos)

    # get all pos from a list of ang QN.
    def __calculate_pdos(self, gpaw_calculator):

        # create system with copy of all atoms and attach lm partial DOS
        system = []
        for cc, atom in enumerate(self.atoms):

            print("Doing atom #{:>3}: {:2} ".format(cc, atom.symbol), end="", flush=True)
            
            # Make new atom
            new_atom = dos_entity()
            new_atom.symbol = atom.symbol
            new_atom.index = cc

            try:
                atom_setup = gpaw.setup_data.SetupData(atom.symbol, "LDA")
            except ValueError:
                print("Invalid symbol or missing setup for this specific atom:")
                print("{}".format(atom.symbol))

            # get list of n and l quantum numbers
            n_j = atom_setup.n_j
            l_j = atom_setup.l_j

            # iterate over all of 'em
            proj = 0 # projector index
            dos = []
            dos_labels = []
            for nn, ll in zip(n_j, l_j):
                for mm in range(2*ll + 1):
                    if nn == -1:
                        continue #ignore if n = -1

                    # iterate over spin
                    for ss in [0, 1]:

                        # get ldos
                        __, ldos = gpaw_calculator.get_orbital_ldos(
                                                                a = cc,
                                                                spin = ss,
                                                                angular = proj,
                                                                npts = self.npoints,
                                                                width = self.width
                                                               )

                        # append ldos and retrieve label
                        dos.append(ldos)
                        dos_labels.append(
                            self.__assign_pdos_labels(nn, ll, mm, ss))

                        print(".", end="", flush=True)
                    proj += 1

            # append new atom to system
            new_atom.dos = np.array(dos)
            new_atom.dos_labels = dos_labels
            system.append(new_atom)

            print(" done.")
            
        return system
    
    # get all pos from a list of ang QN.
    def __calculate_pdos_wip(self, dos_calculator):

        # create system with copy of all atoms and attach lm partial DOS
        system = []
        for cc, atom in enumerate(self.atoms):

            print("Doing atom #{:>3}: {:2} ".format(cc, atom.symbol), end="", flush=True)
            
            # Make new atom
            new_atom = dos_entity()
            new_atom.symbol = atom.symbol
            new_atom.index = cc

            try:
                atom_setup = gpaw.setup_data.SetupData(atom.symbol, "LDA")
            except ValueError:
                print("Invalid symbol or missing setup for this specific atom:")
                print("{}".format(atom.symbol))

            # get max l quantum number
            l_max = np.max(atom_setup.l_j)

            # iterate over all of 'em
            dos = []
            dos_labels = []
            
            # iterate over l quantum number
            for ll in range(l_max):
                # iterate over m quantum number
                for mm in range(2*ll + 1):
                    # iterate over spin
                    for ss in [0, 1]:

                        # get ldos
                        ldos = dos_calculator.raw_pdos(
                                                       energies = self.energies[ss],
                                                       a        = cc,
                                                       l        = ll,
                                                       m        = mm,
                                                       spin     = ss,
                                                       width = self.width
                                                       )

                        # append ldos and retrieve label
                        dos.append(ldos)
                        dos_labels.append(
                            self.__assign_pdos_labels(None, ll, mm, ss))
                        print(".", end="", flush=True)

            # append new atom to system
            new_atom.dos = np.array(dos)
            new_atom.dos_labels = dos_labels
            system.append(new_atom)

            print(" done.")
            
        return system

    # assign labels depending on l, m, s quantum numbers
    def __assign_pdos_labels(self, nqn, lqn, mqn, spin):

        label = {}
        label["n"] = str(nqn)
        label["l"] = ["s", "p", "d", "f"][lqn]
        label["m"] = [[""], ["y", "z", "x"],
                      ["xy", "yz", "z2", "zx", "x2-y2"],
                      ["f1", "f2", "f3", "f4", "f5", "f6", "f7"]][lqn][mqn]
        label["s"] = ["up", "dn"][spin]

        return label

    # convert list of symbols or list of indexes to just a list of indexes
    def __symbols_to_indexes(self, symbols):
            # check if symbols is a list of strings
        if check_list_of_str(symbols):
            if symbols == "all":
                indexes = range(len(self.system))
            else:
                indexes = []
                for atom in self.system:
                    if any([ii == atom.symbol for ii in symbols]):
                        indexes.append(atom.index)
        else:
            indexes = symbols

        return np.array(indexes)

    # group pdos of many atoms, "symbols" can be "all" (all atoms), a list of atom
    # indexes or the symbols of the elements wanted. Return a partial DOS object
    def __group_atoms_pdos(self, symbols="all"):

        # generate index list
        indexes = self.__symbols_to_indexes(symbols)

        # sum relevant pdos
        pdos = np.sum([atom.dos for atom in np.array(self.system)[indexes]], axis=0)
        dos_labels = self.system[indexes[0]].dos_labels

        # create new dos object that is the sum of the atoms requested
        pdos_obj            = dos_entity()
        pdos_obj.dos        = pdos
        pdos_obj.dos_labels = dos_labels
        pdos_obj.natoms     = len(indexes) #how many atoms in the pdos obj for norm

        return pdos_obj

    # group a pdos according to a particular l quantum number. Return a list
    # with the two spin channels
    def __group_l_qn_pdos(self, dosobj, l_qn):

        # spin labels
        s_labels = np.array([ii["s"] for ii in dosobj.dos_labels])
        l_labels = np.array([ii["l"] for ii in dosobj.dos_labels])

        ndos = []
        for spin in ["up", "dn"]:

            indexes = np.where((s_labels == spin) & (l_labels == l_qn))

            # print(indexes)
            ndos.append(np.sum(dosobj.dos[indexes], axis=0))

        return ndos
    
    # group a pdos according to a particular m quantum number. Return a list
    # with the two spin channels
    def __group_l_m_qn_pdos(self, dosobj, l_qn, m_qn=None):

        # spin labels
        s_labels = np.array([ii["s"] for ii in dosobj.dos_labels])
        l_labels = np.array([ii["l"] for ii in dosobj.dos_labels])
        m_labels = np.array([ii["m"] for ii in dosobj.dos_labels])

        ndos = []
        for spin in ["up", "dn"]:
            
            if m_qn is None:
                indexes = np.where((s_labels == spin) & (l_labels == l_qn))
            else:
                indexes = np.where((s_labels == spin) & (l_labels == l_qn) & (m_labels == m_qn))

            # print(indexes)
            ndos.append(np.sum(dosobj.dos[indexes], axis=0))

        return ndos

    def __save_self(self, oname):

        path = "/".join(self.gpw_file.split("/")[:-1]) + "/"

        # check output name
        if oname == "default":
            oname = self.gpw_file.split("/")[-1].split(".")[0] + ".pkl"
        elif not oname.endswith(".pkl"):
            oname = oname.split(".")[0] + ".pkl"


        with open(path + oname, 'wb') as fout:
            pickle.dump(self, fout)

        print("Saved everything as {}".format(oname))
        return


    def get_pdos(self, symbols="all", l_qn="spdf", m_qn=None, norm=False):

        # first get pdos obj for all atoms involved
        pdos_obj = self.__group_atoms_pdos(symbols)

        # select l or m qn
        if m_qn is None:
            # get pdos of l qn
            pdos = np.sum([self.__group_l_qn_pdos(pdos_obj, ll) for ll in l_qn], axis=0)
        else:
            # get pdos of l qn
            pdos = np.sum([self.__group_l_m_qn_pdos(pdos_obj, l_qn, mm) for mm in m_qn], axis=0)

        if norm:
            return pdos/pdos_obj.natoms
        else:
            return pdos
        
    
    def integrate_DOS_to_efermi(self, symbols="all", l_qn="spdf"):
        
        # get pdos
        pdos = self.get_pdos(symbols, l_qn=l_qn)
        
        # check where occupied
        e_idxs = self.energies < self.efermi
        
        integrated_dos = []
        # iterate over spins
        for ii in range(2):
            
            cdos = np.trapz(pdos[ii][e_idxs[ii]], self.energies[ii][e_idxs[ii]]) 
            integrated_dos.append(cdos)
        
        return np.sum(integrated_dos), integrated_dos[0] - integrated_dos[1]
    
    def get_energies(self):
        return self.energies


