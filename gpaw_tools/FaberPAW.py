#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 09:14:45 2021

@author: roncofaber
"""
#%%
# import modules

# ASE
import ase
import ase.neb
import ase.io
from ase.io.trajectory import TrajectoryWriter
from ase.units import Bohr

# GPAW
import gpaw
import gpaw.mpi as mpi
from ase.parallel import parprint, paropen, world

# for CDFT
import gpaw.cdft.cdft
from gpaw_tools.CDFT_faber import CDFT
# from gpaw.cdft.cdft import CDFT
from gpaw.analyse.hirshfeld import HirshfeldPartitioning
from gpaw.analyse.vdwradii import vdWradii
from ase.calculators.vdwcorrection import vdWTkatchenko09prl

# various
import numpy as np
import scipy.optimize as sopt
import subprocess
import dill
import itertools
# import matplotlib.pyplot as plt
# import glob
# import seaborn as sns
#%% aux functions
def as_list(inp):
    return [inp] if not isinstance(inp, list) else inp

#%%
# Use it to attach the calculator to image
def return_calculator(image, gpawargs, oname="-", ranks=None,
                      cdftargs=None, VdW=False):

    # define calculator
    calc = gpaw.GPAW(**gpawargs, communicator = ranks, txt = oname)
    
    # if define charge constrained region, set up also CDFT calc
    if cdftargs is not None:

        calc = CDFT(
            calc  = calc, # if not VdW else mycalc.calculator,
            atoms = image,
            txt   = oname.replace("scf", "cdft"),
            **cdftargs,
            )

        parprint("Generated calculator with " + CDFT.__module__, flush=True)
        
    if VdW:
        radii = vdWradii(image.get_chemical_symbols(), 'PBE')
        calc = vdWTkatchenko09prl(HirshfeldPartitioning(calc), radii)
    
    return calc


# read parameters file from CDFT run to help generate restart object
# with correct parameters
def read_params_file(filename):

    params = {}
    with open(filename, "r") as fin:
        for line in fin:
            cline = line.split()
            tag = cline[0]
            val = float(cline[2])

            params[tag] = val

    return params

# Use to restart cdft calculation from .gpw file and attach the correct
# parameters so that coupling can be calculated
def restart_CDFT_calc(gpw_name, params_name):

    # read gpw file to make new GPAW calc
    gpw_calc = gpaw.GPAW(gpw_name)#, communicator=[world.rank])


    cdft_args = gpw_calc.todict()["external"]

    params = read_params_file(params_name)

    cdft_calc = CDFT(
        calc           = gpw_calc,
        atoms          = gpw_calc.atoms,
        charge_coefs   = [params["Va"]],
        charge_regions = cdft_args["regions"],
        restart        = True
        )

    cdft_calc.constraints      = params["NA"]
    cdft_calc.Ecdft            = params["FA"]
    cdft_calc.Edft             = params["EA"]
    # cdft_calc.n_charge_regions = params["N_charge_regions_A"]

    return cdft_calc

# get distance between different images, if index is defined consider only
# atoms in [index]
def get_distance_between_images(poslist, index=None, initial=False):

    if not initial:
        dist = []
    else:
        dist = [0]
    for cc in range(1,len(poslist)):
        if index is None:
            dd = np.sum(np.linalg.norm(poslist[cc] - poslist[cc-1], axis=1))
        else:
            dd = np.sum(np.linalg.norm(poslist[cc][index] - poslist[cc-1][index], axis=1))

        dist.append(dd)

    return dist if len(dist) > 1 else dist[0]


# generate a image list for NEB starting from an initial and final structure or
# a previous trajectory. nimg is the number of INTERMEDIATE images
def generate_image_list(initial     = None,
                        final       = None,
                        nimg        = 10,   #number of intermediate images
                        trajectory  = None,
                        pbc         = None,
                        constraints = None,
                        magmoms     = None,
                        use_idpp    = True):

    # Read initial and final states if still strings, else it's ok:
    if isinstance(initial, str):
        initial = ase.io.read(initial)
    if isinstance(initial, str):
        final   = ase.io.read(final)

    # just start from initial and final images
    if trajectory is None:

        # Make a band consisting of nimg images and interpolate:
        image_list  = [initial]
        image_list += [initial.copy() for i in range(nimg)]
        image_list += [final]
        ase.neb.interpolate(image_list)

        if use_idpp:
            ase.neb.idpp_interpolate(image_list)

    # there is already a trajectory file, let's read it
    else:

        # check if the traj is still a string, if yes read it
        if isinstance(trajectory, str):
            image_list = ase.io.read(trajectory)
        else:
            image_list = trajectory

        # give clean initial and end images if provided
        if initial is not None:
            image_list[0] = initial
        if final is not None:
            image_list[-1] = final

        # generate image list from traj. (if nimg enough increase_resolution
        # does not add images)
        image_list = increase_resolution(image_list, nimg+2)

    # go through all and put pbcs, constraints, magmoms if any
    for image in image_list:
        if pbc is not None:
            image.set_pbc(pbc)

        if constraints is not None:
            image.set_constraint(constraints)

        if magmoms is not None:
            image.set_initial_magnetic_moments(magmoms)

    return image_list

# increase resolution adding images where the distance between them is max
# until a list of len nimg is reached
def increase_resolution(trajectory, nimg, ato_indexes=None):

    initial = trajectory[0].copy()

    poslist = [ii.get_positions() for ii in trajectory]

    # insert frames until target reached
    while len(poslist) < nimg:
        dist = get_distance_between_images(poslist)

        ind = np.argmax(dist)


        newpos = (poslist[ind+1] + poslist[ind])/2

        poslist.insert(ind+1, newpos)

    # generate image list
    image_list = []
    for pos in poslist:
        image           = initial.copy()
        image.positions = pos
        image_list.append(image)

    return image_list

def interpolate_with_lambda(initial, final, lam):

    assert all(initial.symbols == final.symbols), "Structures are different!"

    new_structure = initial.copy()

    positions = (1-lam)*initial.get_positions() + lam*final.get_positions()

    new_structure.set_positions(positions)


    return new_structure

def interpolate_cdft_arguments(cdftargs1, cdftargs2, nimages):

    new_args = []
    for lam in np.linspace(0,1,nimages):

        temp_arg = cdftargs1.copy()


        chg1 = np.array(cdftargs2["charge_coefs"])
        chg2 = np.array(cdftargs1["charge_coefs"])

        temp_arg["charge_coefs"] = lam*chg1 + (1-lam)*chg2

        if "bounds" in cdftargs1.keys():
            bnd1 = np.array(cdftargs2["bounds"])
            bnd2 = np.array(cdftargs1["bounds"])

            temp_arg["bounds"] = [tuple(ii) for ii in lam*bnd1 + (1-lam)*bnd2]

            new_args.append(temp_arg)

    return new_args

# prepare a NEB object with all calculators attached
def setup_neb_object(image_list, calculator, oname, parallel=False,
                     cdftargs=None, VdW=False, extrema=False):

    if extrema:
        # all images
        nimg    = len(image_list)
        iishift = 0
        system  = []

    else:
        # only intermediate images
        nimg    = len(image_list) - 2
        iishift = 1

        # attach first image without calculator
        system = [image_list[0]]

    # check if cdftargs is a list and make a list of it
    if isinstance(cdftargs, list):
        cdftargs = interpolate_cdft_arguments(cdftargs[0], cdftargs[1], nimg)
    elif isinstance(cdftargs, dict):
        cdftargs = interpolate_cdft_arguments(cdftargs, cdftargs, nimg)
    else:
        assert cdftargs is None, "Check your CDFT arguments"
        cdftargs = nimg*[None]

    # number of cpus per image
    ncpu = mpi.size // nimg

    # attach calculators to images in the middle
    for ii in range(nimg):

        image = image_list[ii+iishift]

        filename = "scf_{}_{}.out".format(oname, str(ii+iishift).zfill(2))

        if parallel:

            ranks = range(ii * ncpu, (ii + 1) * ncpu)
            # only attach on some ranks
            if mpi.rank in ranks:


                image.calc = return_calculator(image, calculator, filename,
                                           ranks=ranks, cdftargs=cdftargs[ii],
                                           VdW=VdW)

        # serial
        else:
            image.calc = return_calculator(image, calculator, filename,
                                           ranks=None, cdftargs=cdftargs[ii],
                                           VdW=VdW)

        system.append(image)

    if not extrema:
        # add last image
        system.append(image_list[-1])


    # write something on the terminal, looks cool and give some info to the
    # user
    if parallel:
        assert nimg * (mpi.size // nimg) <= mpi.size
        print_initial_parneb(nimg, mpi.size // nimg)

    return system


# print nicely the start of the calculation
def print_initial_parneb(nimg, ncpu):
    parprint("# ========================================================== #")
    parprint("# Starting a parallel calculation with {:2} images on {:2} cpus: #".format(nimg, mpi.size))
    parprint("#      >>>>  Parallelizing with {:2} cpus per image  <<<<      #".format(ncpu))
    parprint("# ========================================================== #")
    return


def calculate_charge_density(system):

    # calculate electron density
    density = system.calc.get_all_electron_density(gridrefinement=4) * Bohr**3

    totimg = len(system)
    ncpu = mpi.size // totimg

    for ii in range(totimg):
        ranks = range(ii * ncpu, (ii + 1) * ncpu)
        if mpi.rank in ranks:
            try:
                ase.io.write('charge_density_{}.cube'.format(str(ii).zfill(2)),
                         system, data=density, parallel=False)
            except:
                parprint("Skipped rank #{}".format(mpi.rank))

# setup an AutoNEB system TODO
def setup_AutoNEB(image_list, gpawargs, aNEB_prefix, cdftargs=None, VdW=False):

    for cc, image in enumerate(image_list):

        aNEB_name = aNEB_prefix + str(cc).zfill(3) + ".traj"

        oname = "aneb_setup_{}.out".format(str(cc).zfill(3))
        image.calc = return_calculator(image=image,
                                    gpawargs=gpawargs,
                                    oname=oname,
                                    cdftargs=cdftargs,
                                    VdW=VdW)
        image.get_potential_energy()

        writer = TrajectoryWriter(aNEB_name,
                                  mode="w",
                                  atoms=image,
                                  properties=["energy"])

        writer.write()

# translate and rotate system 2 to have its atoms of indexes "atom_indexes"
# as close to system 1
def rearrange_systems(sys1, sys2, atom_indexes=None, center_rot="COM", onlyrot=False):

    def optimize_rotation(X, system1, system2, center_rot="COM", index=None):

        # unpack the load
        if len(X) == 6:
            phi, theta, gamma, x, y, z = X
        else:
            phi, theta, gamma = X
            x, y, z = [0, 0, 0]

        # create copy to not mess stuff up
        s2 = system2.copy()

        # translate
        s2.positions += [x, y, z]

        # rotate around center
        s2.rotate(phi,   [1, 0, 0], center=center_rot)
        s2.rotate(theta, [0, 1, 0], center=center_rot)
        s2.rotate(gamma, [0, 0, 1], center=center_rot)

        # get new distance bet. images... will it be better?
        dist = get_distance_between_images([system1.positions, s2.positions], index)

        return dist

    # optimize rotation and translation

    # if move to center
    if onlyrot:
        optres = sopt.minimize(optimize_rotation, [0, 0, 0],
                                     args=(sys1, sys2, center_rot, atom_indexes))
    else:

        optres = sopt.minimize(optimize_rotation, [0, 0, 0, 0, 0, 0],
                                     args=(sys1, sys2, center_rot, atom_indexes))

    angles = optres.x[:3]
    translation_vector = optres.x[3:] if not onlyrot else [0, 0, 0]

    nsys = sys2.copy()

    nsys.translate(translation_vector)

    nsys.rotate(angles[0], [1, 0, 0], center=center_rot)
    nsys.rotate(angles[1], [0, 1, 0], center=center_rot)
    nsys.rotate(angles[2], [0, 0, 1], center=center_rot)

    return nsys, angles, translation_vector



# only works for cDFT, print charges as calculated from cDFT
def print_charges(system=None, oname='charge_Hirshfeld.xyz', append=True):

    system.set_initial_charges(system.get_atomic_numbers() -
                          system.calc.get_number_of_electrons_on_atoms())
    ase.io.write(oname, system, append=append)

    return

# run bader analysis on .cube file, gives ACF, ACV, BCF files
def run_bader(chargefile):

    with paropen('bader.log', 'w') as fout:
        if world.rank == 0:
            command = ["bader", chargefile]
            subprocess.run(command, shell=False, stdout=fout)
        else:
            command = None
        world.barrier()

    return

# given a polaron site, find indexes of S ring they belong to
def find_sulphur_ring(dist_site, system):
    S_indexes = np.where(np.array(system.get_chemical_symbols()) == "S")
    L_ring = [dist_site[0]]
    R_ring = [dist_site[-1]]
    Lidx = dist_site[0]
    Ridx = dist_site[-1]
    while len(L_ring) + len(R_ring) < 8:

        nextL = np.argsort(system.get_distances(Lidx, S_indexes, mic=True))[:3]
        nextR = np.argsort(system.get_distances(Ridx, S_indexes, mic=True))[:3]

        for idx in nextL:
            if idx not in L_ring and idx not in R_ring:
                L_ring.append(idx)
                Lidx = idx
        for idx in nextR:
            if idx not in L_ring and idx not in R_ring:
                R_ring.append(idx)
                Ridx = idx

    ring_indexes = L_ring + R_ring[::-1]

    return ring_indexes


# legacy, should not work anymore -----------------------------------
# def print_hirshfeld_charges(system, do_cube=False,
#                             oname='charge_density.cube'):

#     # write Hirshfeld charges out
#     hf = gpaw.analyse.hirshfeld.HirshfeldPartitioning(system.calc)
#     for atom, charge in zip(system, hf.get_charges()):
#         atom.charge = charge

#     ase.io.write('hirshfeld_charges.xyz', system)

#     # also do cube file
#     if do_cube:
#         # create electron density cube file ready for bader
#         density = system.calc.get_all_electron_density(gridrefinement=4)
#         ase.io.write(oname, system, data=density*Bohr**3)

#     return


# postporcess a converged calculation to print Hirshfeld charges or
# generate cube file. Does NOT work with CDFT yet.
def postprocess_gpw_file(system,
                         do_hirshfeld = False,
                         do_cube      = False,
                         do_bands     = False,
                         cname        = "charge_density.cube",
                         hname        = "hirshfeld_charge.xyz",
                         bname        = "band_n",
                         grid         = 4,
                         band_indexes = None,
                         kpoints      = 0,
                         ):

    if do_hirshfeld:
        try:
            # write Hirshfeld charges out
            hf = gpaw.analyse.hirshfeld.HirshfeldPartitioning(system.calc)
            for atom, charge in zip(system, hf.get_charges()):
                atom.charge = charge

            ase.io.write(hname, system)
        except:
            parprint("Failed to calculate Hirshfeld charges!")


    # also do cube file
    if do_cube:
        try:
            # create electron density cube file ready for bader
            density = system.calc.get_all_electron_density(gridrefinement=grid)
            ase.io.write(cname, system, data=density*Bohr**3)
        except:
            parprint("Failed to generate cube file!")

    if do_bands:
        try:
            # plot bands
            print_bands_as_cube(system, band_indexes=band_indexes, oname=bname,
                                kpoints=kpoints)
        except:
            parprint("Failed to print your bands!")

    return

# print bands of a system as cube files
def print_bands_as_cube(system, band_indexes=None, oname="band",
                        kpoints=0):

    # get calculator
    try:
        calc = system.calc.calc
    except:
        calc = system.calc

    if band_indexes is None:
        nbands = calc.get_number_of_bands()
        band_indexes = range(nbands)

    # check number of spins
    nspins = calc.get_number_of_spins()

    if nspins == 2:
        spins = [0, 1]
        splab = ["_up", "_dn"]
    else:
        spins = [0]
        splab = [""]

    # make sure kpoints is list
    kpoints = as_list(kpoints)

    # loop over all wfs and write their cube files
    for band, kpt, spin in itertools.product(band_indexes, kpoints, spins):

        # current output name
        c_oname = oname + "_n{}_k{}{}".format(str(band).zfill(3),
                                              str(kpt).zfill(2),
                                              splab[spin])

        # Update us with awe
        parprint("Calculating wavefunction no: {}, "\
                 "kpt: {}, spin: {}".format(band,
                                            kpt,
                                            spin))

        # get wavefunction
        wf = calc.get_pseudo_wave_function(band=band, spin=spin, kpt=kpt)

        # square it
        wf_sq = (wf*np.conj(wf)).real

        # get max
        idx_max   = np.argmax(wf_sq)
        wf_sq_max = wf_sq.flat[idx_max]

        # calculate phase norm
        phi = wf.flat[idx_max]/wf_sq_max

        # calculate sign of squared WF
        segno = np.sign(np.real(wf*np.conj(phi)))

        # calculate wavefunction
        wavefunction = segno*wf_sq * Bohr**1.5

        ase.io.write(c_oname + ".cube", system, data=wavefunction)


    return


# Class that writes the wavefunctions of a system during relaxations
# (could be expanded more...). Initialize before the relaxation and then attach
# as following:
    # relax = FIRE(whatever you need here)
    # observer = WFS_writer(name, intervals)
    # relax.attach(observer.write, intervals, system)

class WFS_writer:
    def __init__(self, name, intervals):

        self.ii = 0
        self.n   = name
        self.int = intervals

    def write(self, system):
        system.calc.write('{}_wfs_n{}.gpw'.format(self.n,
                                                  str(self.ii).zfill(3)),
                          mode='all')
        self.ii += self.int

# use to save electrostatic potential (need for CDFT cause)
def save_electrostatic_potential(system, oname="electrostatic_potential.pkl"):

    # get calculator
    try:
        calc = system.calc.calc
    except:
        calc = system.calc

    try:
        ele_pot = calc.get_electrostatic_potential()
    except:
        parprint("Could not compute electrostatic potential.")
        return

    with open(oname, "wb") as fout:
        dill.dump(ele_pot, fout)

    return
