#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 14:08:41 2022

@author: roncoroni
"""

import numpy as np
import ase
import ase.io
import itertools
import psutil

from scipy import spatial

#%%

def how_many_jobs(parallel=True):
    if not parallel:
        return 1
    else:
        return psutil.cpu_count(logical = False) - 2

def as_list(inp):
    return [inp] if not isinstance(inp, list) else inp.copy()

def make_LiCS_supercell(system, increase_size):

    P = np.eye(3)
    for cc, incsiz in enumerate(increase_size):
        P[cc,cc] = incsiz

    system = ase.build.make_supercell(system, P)

    Li_idxs   = np.where([cc == "Li" for cc in system.get_chemical_symbols()])[0]
    del system[Li_idxs[1:]]

    return system

# return list of indexes from mixed input of indexes and string (elements)
def atoms_to_indexes(system, symbols):

    # check if symbols is a list of strings
    if symbols == "all":
        return list(range(len(system.get_chemical_symbols())))

    if not isinstance(symbols, list):
        symbols = [symbols]

    indexes = []
    for symbol in symbols:
        if not isinstance(symbol, str):
            indexes.append(symbol)
        else:
            for cc, atom in enumerate(system.get_chemical_symbols()):
                if atom == symbol:
                    indexes.append(cc)

    return indexes

def make_grid_from_scaled_positions(cell, x, y, z):

    x = np.asarray(x).reshape(-1)
    y = np.asarray(y).reshape(-1)
    z = np.asarray(z).reshape(-1)


    XYZ = np.array([np.dot(cell.T, [xx, yy, zz]) for zz, yy, xx in itertools.product(z,y,x)])

    return XYZ

def find_nearest(array, value, index=False):
    distance, idx = spatial.KDTree(array).query(value)

    if index:
        return idx
    else:
        return array[idx]