"""User provided customizations.
    Customized by F. Roncoroni on the 09.05.2022
    for the HPC cluster
"""

# compilers + extra arguments
compiler    = 'mpiicc'  # was mpicc, maybe if it doesn't work change to that one?
mpicompiler = 'mpiicc'  # use None if you don't want to build a gpaw-python
mpilinker   = 'mpiicc'

extra_compile_args += [
    '-O3',
    "-fPIC",
    "-ipo",
    "-xHOST",
    "-fast",
    "-std=c99",
    "-gcc-name=/global/software/sl-7.x86_64/modules/langs/gcc/9.3.0/bin/gcc" #solves Elpa
    ]

extra_link_args += [
#        "-lfftw3_mpi", # for mpi FFTW? Not sure it's supported keep an eye on it...
#        "-lfftw3",
        "-lm",
]

# macros (tbh not sure what they do but copied from other siteconfig.py files)
define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', '1')]
define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', '1')]
define_macros += [("GPAW_ASYNC",1)]
define_macros += [("GPAW_MPI2",1)]
define_macros += [('GPAW_MKL','1')]

# MKL
scalapack = True
mkl = "/global/software/sl-7.x86_64/modules/langs/intel/parallel_studio_xe_2020_update4_cluster_edition/compilers_and_libraries_2020.1.217/linux/mkl/"

include_dirs += [mkl + 'include']
library_dirs += [mkl + 'lib/intel64']
extra_link_args += ['-Wl,-rpath,' + mkl + 'lib/intel64']

for lib in ['mkl_intel_lp64',
            'mkl_sequential',
            'mkl_core',
            'mkl_scalapack_lp64',
            'mkl_blacs_intelmpi_lp64',
            'pthread',]:
    if lib not in librearies:
	libraries.append(lib)

# FFTW3:
fftw = True
fftwlib = '/global/home/groups/nano/share/software/fftw3_3.3.10/'
extra_link_args += ['-Wl,-rpath={fftw}/lib'.format(fftw=fftwlib)]
include_dirs += [fftwlib + 'include']
library_dirs += [fftwlib + 'lib']
if 'fftw3' not in libraries:
    libraries.append('fftw3')


# Libxc
xc = '/global/home/groups/nano/share/software/libxc_5.2.2/'
include_dirs += [xc + 'include']
library_dirs += [xc + 'lib']
# You can use rpath to avoid changing LD_LIBRARY_PATH:
extra_link_args += ['-Wl,-rpath={xc}/lib'.format(xc=xc)]
if 'xc' not in libraries:
    libraries.append('xc')

elpa = True
elpadir = '/global/home/groups/nano/share/software/elpa_2021.11.002/'
libraries += ['elpa']
library_dirs += ['{}/lib'.format(elpadir)]
extra_link_args += ['-Wl,-rpath={}/lib'.format(elpadir)]
include_dirs += [
    '/global/home/groups/nano/share/software/elpa_2021.11.002/include/elpa-2021.11.002/',
#    '/global/home/groups/nano/share/software/elpa_2021.11.002/include/elpa-2021.11.002/elpa/'
    ]

