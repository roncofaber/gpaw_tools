#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 09:48:05 2021

@author: roncofaber
"""

import gpaw_tools.FaberDOS as fad
from gpaw import setup_paths # need to add custom setups for XAS
import argparse
import os, sys
#%%


parser = argparse.ArgumentParser()


parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="default", type=str)

parser.add_argument("-n", "--npoints", dest="npoints",
                    help="choose how many points in the DOS energy line",
                    default=2001, type=int)

parser.add_argument("-w", "--width", dest="width",
                    help="widht of the DOS gaussians",
                    default=0.1, type=float)

parser.add_argument("-sp", "--setup_path", dest="setup_path",
                    help="add additional setup potential path",
                    default=None, type=str)

# use WIP feature
parser.add_argument("-wip", "--wip", dest="wip", nargs="?",
                    help="Use to use WIP feature",
                    default=False, type=bool, const=True)


def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)
    
    if attributes.setup_path is not None:
        setup_paths.insert(0, attributes.setup_path)

    fad.myDOS(os.getcwd() + "/" + sys.argv[1],
              npoints  = attributes.npoints,     # number of points in dos
              spinpol  = True,     # TODO
              save     = True,     # save or not as pickle
              savename = attributes.outname, # name of the pickle object
              width    = attributes.width,
              wip      = attributes.wip
              )

if __name__ == '__main__':
    main()
