#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:00:13 2021

@author: roncofaber
"""

import subprocess
import argparse
from joblib import Parallel, delayed
import psutil
import ase.io
from ase.io.bader import attach_charges
import numpy as np
import matplotlib.pyplot as plt
import os
#%%

class Bader():

    def __init__(self, attributes):

        if type(attributes) is dict:
            self.__dict__ = attributes.copy()
        else:
            self.__dict__ = attributes.__dict__.copy()

        self.nfiles = len(self.filename)

        return

    def run_script_from_shell(self):

        if self.new:
            self.bader_analysis()

        if os.path.isdir("bader_analysis"):
            self.rootdir = "bader_analysis/"
        else:
            self.rootdir = ""

        if self.sequence:
            self.generate_charge_files()

        if self.readcharges:
            self.charges, self.labels, self.poslist = self.read_charges()

            if self.plot:
                self.plot_charges()

        if self.plot_barrier:

            self.energies = self.get_energy_barrier()
            self.plot_energy_barrie()


        return

    # perform bader analysis of a filename..cube file (works in parallel)
    def bader_analysis(self):

        def single_run(fn, cc=0):

            appx = str(cc).zfill(3)
            tmpdir = "tmp_{}".format(appx)

            subprocess.run("mkdir -p {}".format(tmpdir), shell=True,
                           cwd="bader_analysis")
            subprocess.run("bader ../../{} > bader_log.dat".format(fn), shell=True,
                           cwd="bader_analysis/" + tmpdir)

            for fl in ["ACF", "AVF", "BCF", "bader_log"]:
                subprocess.run("mv {}.dat ../{}_{}.dat".format(fl, fl, appx),
                               shell=True, cwd="bader_analysis/" + tmpdir)

            subprocess.run("rm -r {}".format(tmpdir), shell=True,
                           cwd="bader_analysis")
            return

        # create main directory
        subprocess.run("mkdir -p bader_analysis", shell=True)

        if len(self.filename) == 1:
            single_run(self.filename[0])
        else:
            num_cores = psutil.cpu_count(logical = False) - 1
            Parallel(n_jobs=num_cores)(delayed(single_run)(fn, cc)
                                       for cc, fn in enumerate(self.filename))

    # takes output of bader_analysis and performs bader_gpaw from Erpan
    def generate_charge_files(self):

        def single_bader_gpaw(fn, cc=0):
            appx = str(cc).zfill(3)

            atoms = ase.io.read(fn)
            attach_charges(atoms, self.rootdir + "ACF_{}.dat".format(appx))

            elm_set = set(atoms.get_chemical_symbols())
            elm     = dict.fromkeys(elm_set, 0)

            total_charge = elm.copy()


            for atom in atoms:
                total_charge[atom.symbol] += atom.charge

            atoms.set_pbc(self.pbc)
            atoms.write(self.rootdir + "bader_charge_{}.xyz".format(appx))

            # for ii in sorted(total_charge.keys()):
            #     print(' ', ii, total_charge[ii], end='')
            # print(' sum {}'.format(sum(total_charge.values())))
            return

        if len(self.filename) == 1:
            single_bader_gpaw(self.filename[0])
        else:

            for cc, fn in enumerate(self.filename):
                single_bader_gpaw(fn, cc)

            # num_cores = int(multiprocessing.cpu_count()/2)
            # Parallel(n_jobs=num_cores)(delayed(single_bader_gpaw)(fn, cc)
                                       # for cc, fn in enumerate(self.filename))

        return

    # read gparser and return energies of NEB
    def get_energy_barrier(self):

        def parse_energy(filename):
            linetoparse = "gparser.py -i {} -cfo".format(filename)

            command = subprocess.run(linetoparse,
                                    shell=True,
                                    cwd=self.rootdir,
                                    stdout=subprocess.PIPE)

            energies = command.stdout.decode("utf-8").strip().split("\n ")

            outlist = []
            for item in energies:
                if not item:
                    print("found a crappy item")
                elif item.startswith("np.NaN"):
                    # print("here nan")
                    outlist.append(np.NaN)
                else:
                    # print("just here")
                    outlist.append(float(item))

            print("parsed {}".format(filename))

            return outlist

        energies = parse_energy("../scf_*.out")

        energies = 1000*(energies - np.nanmin(energies))

        return energies

    def get_position_along_path(self, pos):

        dist = [0]
        for cc, pp in enumerate(pos[1:]):

            dist.append(dist[-1] + np.linalg.norm(pp - pos[cc]))

        return dist/np.max(dist)


    def read_charges(self):


        filename = self.rootdir + "bader_charge_{}.xyz"
        chargelist = []
        poslist    = []
        for cc in range(self.nfiles):
            ifile = filename.format(str(cc).zfill(3))

            system = ase.io.read(ifile)

            charge   = system.get_initial_charges()
            position = system.get_positions()

            chargelist.append(charge)
            poslist.append(position)

        labels = system.get_chemical_symbols()

        return np.array(chargelist).T, labels, poslist

    def plot_charges(self):

        fs = 13
        ms = 6
        lw = 2

        pos   = np.array(self.poslist)[:, self.atom]
        xdata = self.get_position_along_path(pos)


        fig, ax1 = plt.subplots(figsize=(7,5))
        for cc, char in enumerate(self.charges):


            ax1.plot(
                xdata,
                char,
                linestyle='-',
                marker='o',
                markersize=ms,
                linewidth=lw,
                label=r"{}{}".format(self.labels[cc], cc)
                )


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'charge per atom [q]', fontsize=fs, fontweight="bold")

        plt.legend(bbox_to_anchor=(1.01, 1), loc='upper left')
        plt.tight_layout()
        plt.show()
        return

    def plot_energy_barrie(self):
        fs = 13
        ms = 7


        fig, ax1 = plt.subplots(figsize=(7,5))


        pos   = np.array(self.poslist)[:, self.atom]
        xdata = self.get_position_along_path(pos)


        ax1.plot(
                xdata,
                self.energies,
                # color='r',
                linestyle='--',
                marker='o',
                markersize=ms)


        ax1.set_xlabel(r'reaction coordinate [-]', fontsize=fs, fontweight="bold")
        ax1.set_ylabel(r'$\Delta$E [meV]', fontsize=fs, fontweight="bold")

        plt.tight_layout()
        plt.show()












#%% parsing arguments

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--filename", dest="filename", nargs='+',
                    help="input file name",
                    default=None, type=str)

parser.add_argument("-n", "--new", dest="new", nargs='?',
                    help="run a new analysis from scratch",
                    default=False, type=str2bool, const=True)

parser.add_argument("-s", "--sequence", dest="sequence", nargs='?',
                    help="run second part of code",
                    default=False, type=str2bool, const=True)

parser.add_argument("-r", "--readcharges", dest="readcharges", nargs='?',
                    help="read charge.xyz files",
                    default=False, type=str2bool, const=True)

parser.add_argument("-p", "--plot", dest="plot", nargs='?',
                    help="plot charges",
                    default=False, type=str2bool, const=True)

parser.add_argument("-b", "--plot_barrier", dest="plot_barrier", nargs='?',
                    help="plot energy barrier",
                    default=False, type=str2bool, const=True)

parser.add_argument("-a", "--atom", dest="atom", nargs=1,
                    help="reference atom for plot",
                    default=0, type=int)

parser.add_argument("-pbc", "--pbc", dest="pbc", nargs='+',
                    help="set pbc",
                    default=False, type=str2bool)


#%% main
if __name__ == "__main__":

    attributes = parser.parse_args()

    obj = Bader(attributes)

    obj.run_script_from_shell()