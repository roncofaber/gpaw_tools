#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 10:28:56 2021

@author: roncofaber

This script reads a trajectory file .traj and saves the last snapshot as a
CONTCAR file (VASP format) in direct coordinates
"""

import ase
from ase.io.trajectory import TrajectoryReader
from ase import io
import sys
import argparse

#%% open file and save it as a ase system

parser = argparse.ArgumentParser()

parser.add_argument("-c", "--comment", dest="comment",
                    help="add comment to output file in the 1st line",
                    default=None, type=str)

parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="CONTCAR", type=str)



def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)

    input_name = sys.argv[1]
    output_name = attributes.outname
    
    system  = ase.io.read(input_name)
    #system = TrajectoryReader(input_name)[-1]

    # write output
    ase.io.write(output_name, system, format="vasp", direct=True, vasp5=True, ignore_constraints=True)

    if attributes.comment is not None:
        with open(output_name, "r") as fin:
            lines = fin.readlines()

        lines[0] = attributes.comment + "\n"

        with open(output_name, "w") as fout:
            fout.writelines(lines)


if __name__ == '__main__':
    main()
