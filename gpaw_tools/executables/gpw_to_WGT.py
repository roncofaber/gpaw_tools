#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 18:28:17 2022

@author: roncoroni
"""

import gpaw_tools.FaberWGT as faw
from gpaw import setup_paths # need to add custom setups for XAS
import argparse
import os, sys
#%%


parser = argparse.ArgumentParser()


parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="default", type=str)

parser.add_argument("-sp", "--setup_path", dest="setup_path",
                    help="add additional setup potential path",
                    default=None, type=str)



def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)
    
    if attributes.setup_path is not None:
        setup_paths.insert(0, attributes.setup_path)

    faw.DOS_object(os.getcwd() + "/" + sys.argv[1],
              savename = attributes.outname, # name of the pickle object
              )

if __name__ == '__main__':
    main()
