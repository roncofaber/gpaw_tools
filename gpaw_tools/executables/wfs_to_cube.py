#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  2 07:15:19 2021

@author: roncofaber
"""

import gpaw
import ase
import ase.io
from ase.units import Bohr
import sys



if len(sys.argv) == 1:
    raise "Error, no wavefile selected"
elif len(sys.argv) == 2:
    wavelist = [sys.argv[1]]
else:
    wavelist = sys.argv[1:]



print(wavelist)

for cc, wavename in enumerate(wavelist):

    ind = str(cc).zfill(2)

    wavefile = wavename.format(ind)

    system, calc = gpaw.restart(wavefile)

    density = calc.get_all_electron_density(gridrefinement=4) * Bohr**3
    ase.io.write('charge_density_{}.cube'.format(ind), system, data=density)

