#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 10:19:07 2022

@author: roncoroni
"""

import ase
import ase.io
import sys

#%%

assert len(sys.argv) >= 3, "Not long enough"

trajectory_names = sys.argv[1:-1]
output_name      = sys.argv[-1]


outtraj = ase.io.Trajectory(output_name, "w")

for trajname in trajectory_names:

    ctraj = ase.io.Trajectory(trajname)

    for atoms in ctraj:

        outtraj.write(atoms)

outtraj.close()