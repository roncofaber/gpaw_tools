#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 15:51:41 2021

@author: roncofaber
"""

import gpaw_tools.FaberPAW as fap
import gpaw
from gpaw import setup_paths # need to add custom setups for XAS
import argparse
import sys, os
#%%
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

# def ranger(v):

#     if all([type(ii) is int for ii in v]):
#         return v
#     else:




#     return

#%%

parser = argparse.ArgumentParser()


parser.add_argument("-cn", "--cname", dest="cname",
                    help="choose name output cube file",
                    default="charge_density.cube", type=str)

parser.add_argument("-hn", "--hname", dest="hname",
                    help="choose name output hirshfeld file",
                    default="hirshfeld_charge.xyz", type=str)

parser.add_argument("-bn", "--bname", dest="bname",
                    help="choose name of output bands files",
                    default="band_n", type=str)

parser.add_argument("-hf", "--hirshfeld", dest="hirshfeld", nargs='?',
                    help="Do Hirshfeld analysis",
                    default=False, type=str2bool, const=True)

parser.add_argument("-c", "--cube", dest="cube", nargs='?',
                    help="Print also cube file",
                    default=False, type=str2bool, const=True)

parser.add_argument("-b", "--bands", dest="bands", nargs='?',
                    help="Generate cube files for the bands",
                    default=False, type=str2bool, const=True)

parser.add_argument("-g", "--gridrefinement", dest="gridrefinement", nargs='?',
                    help="grid refinement for cube file",
                    default=4, type=int)

parser.add_argument("-sp", "--setup_path", dest="setup_path",
                    help="add additional setup potential path",
                    default=None, type=str)

parser.add_argument("-bi", "--band_indexes", dest="band_indexes", nargs='+',
                    help="Which bands to select",
                    default=None, type=int)#, action=ranger)

parser.add_argument("-k", "--kpoints", dest="kpoints", nargs='+',
                    help="whihc kpoints operate to",
                    default=0, type=int)


def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)

    if attributes.setup_path is not None:
        setup_paths.insert(0, attributes.setup_path)




    system, __ = gpaw.restart(os.getcwd() + "/" + sys.argv[1])

    # print(attributes.band_indexes)
    fap.postprocess_gpw_file(system,
                             do_hirshfeld = attributes.hirshfeld,
                             do_cube      = attributes.cube,
                             do_bands     = attributes.bands,
                             cname        = attributes.cname,
                             hname        = attributes.hname,
                             bname        = attributes.bname,
                             grid         = attributes.gridrefinement,
                             band_indexes = attributes.band_indexes,
                             kpoints      = attributes.kpoints
                             )


if __name__ == '__main__':
    main()