#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 14:41:36 2021

@author: roncofaber
"""
import os
import sys
import argparse

#%%
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
def restricted_float_0_1(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not a floating-point literal" % (x,))

    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x
def restricted_float(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not a floating-point literal" % (x,))
    return x
#%%

class gpaw_parser():

    def __init__(self, attributes):

        if type(attributes) is dict:
            self.__dict__ = attributes.copy()
        else:
            self.__dict__ = attributes.__dict__.copy()

        if len(self.filename) == 1:
            self.filename = self.filename[0]
            single_file = 1
        else:
            single_file = 0

        # input file is only one
        if single_file:
            if self.recursive:
                self.recursive_run(self.filename)
            else:
                self.single_run(self.filename)

        # more than one input file
        else:
            for fn in self.filename:
                if self.recursive:
                    self.recursive_run(fn)
                else:
                    self.single_run(fn)



        return


    def parse_file(self, input_file):
        enerlist = []
        with open(input_file, "r") as fin:

            for line in fin:

                if input_file.split("/")[-1].startswith("cdft"):
                    if line.startswith("Final DFT energy"):
                        enerlist.append(float(line.split()[4].strip()))
                else:
                    if line.startswith("Free energy:"):
                        enerlist.append(float(line.split(":")[1].strip()))

        return enerlist


    def single_run(self, input_file):
        enerlist = self.parse_file(input_file)
        self.print_output(enerlist)

        return

    def recursive_run(self, input_file):
        rootdir = os.getcwd()

        # get all subdirs
        subdirs = [ii for __, ii, __ in os.walk(rootdir)][0]
        subdirs.sort()

        for subdir in subdirs:
            new_input = subdir + "/" + input_file

            if os.path.exists(new_input):
                enerlist = self.parse_file(new_input)

                try:
                    self.print_output(enerlist)
                except:
                    print("not this")
        return


    def neb_run(self):

        return


    def print_output(self, enerlist):

        if self.formout:
            try:
                line = "{:11.6f}".format(enerlist[-1])
                print(line)
            except:
                print(" np.NaN")

        elif self.cformout:
            for cc, energy in enumerate(enerlist):
                try:
                    line = "{:11.6f}".format(energy)
                    print(line)
                except:
                    print("  np.NaN")

        elif self.supershort:
            try:
                line = "{:11.6f},".format(enerlist[-1])
                print(line)
            except:
                print("  np.NaN,")

        elif self.short:
            try:
                line = "{: 4}| Energy:  {:11.6f}".format(-1, enerlist[-1])
                print(line)
            except:
                print("No energy data available.")

        else:

            for cc, energy in enumerate(enerlist):

                line = "{: 4}| Energy:  {:11.6f}".format(cc, energy)

                print(line)

        return

#%% parsing arguments
parser = argparse.ArgumentParser()

parser.add_argument("-s", "--short", dest="short", nargs='?',
                    help="print only last line",
                    default=False, type=str2bool, const=True)

parser.add_argument("-ss", "--supershort", dest="supershort", nargs='?',
                    help="print only last line",
                    default=False, type=str2bool, const=True)

parser.add_argument("-fo", "--formout", dest="formout", nargs='?',
                    help="print for using in another function",
                    default=False, type=str2bool, const=True)

parser.add_argument("-cfo", "--cformout", dest="cformout", nargs='?',
                    help="complete print for using in another function",
                    default=False, type=str2bool, const=True)

parser.add_argument("-r", "--recursive", dest="recursive", nargs='?',
                    help="run recursively on all eligible subfolders",
                    default=False, type=str2bool, const=True)

parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="CONTCAR", type=str)

parser.add_argument("-i", "--filename", dest="filename", nargs='+',
                    help="input file name",
                    default=["scf.out"], type=str)



#%% main
if __name__ == "__main__":

    attributes = parser.parse_args()

    gpaw_parser(attributes)