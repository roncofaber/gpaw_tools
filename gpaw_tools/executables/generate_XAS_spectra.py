#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 17:26:45 2022

@author: roncoroni
"""

import gpaw_tools.FaberXAS as fax
import argparse
import sys, os
#%%
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

#%%

parser = argparse.ArgumentParser()


parser.add_argument("-p", "--path", dest="path",
                    help="Path to the XAS main directory",
                    default=os.getcwd()+"/", type=str)

parser.add_argument("-sp", "--setup_path", dest="setup_path",
                    help="Path to the setup files for fch calculations",
                    default=None, type=str)

parser.add_argument("-wfs", "--wfs_file", dest="wfs_file",
                    help="Name of the wfs files to consider",
                    default=None, type=str)

parser.add_argument("-log", "--log_file", dest="log_file",
                    help="Name of the LOG files to consider (with dks energy)",
                    default="LOG", type=str)

parser.add_argument("-m", "--multiple", dest="multiple", nargs='?',
                    help="Use if spectra has to be done on subfolders and combined",
                    default=False, type=str2bool, const=True)

parser.add_argument("-xch", "--xch_spin", dest="xch_spin", nargs='?',
                    help="Use 0 or 1 if need to get XCH spectra (one extre e- on one channel)",
                    default=None, type=int)


def main(argv=sys.argv):
    
    # parse arguments
    attributes = parser.parse_args(argv[1:])
    
    if not attributes.path.endswith("/"):
        attributes.path += "/"

    fax.XAS_spectra(
                basepath = attributes.path,
                setpath  = attributes.setup_path,
                wfs_name = attributes.wfs_file,
                logfile  = attributes.log_file,
                multiple = attributes.multiple,
                oname    = "default",
                save     = True,
                restart  = False,
                xch_spin = attributes.xch_spin,
                )



if __name__ == '__main__':
    main()