#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 20:52:05 2021

@author: roncofaber
"""

import gpaw
# for CDFT
import gpaw.cdft.cdft
import numpy as np
import matplotlib.pyplot as plt
import sys

try:
    file = sys.argv[1]
except IndexError:
    print('supply .gpw file as the 1st argument.')
    sys.exit()

system, calc = gpaw.restart(file)

# check if calc is CDFT or not
if type(calc) is gpaw.cdft.cdft.CDFT:
    calculator = calc.calc
else:
    calculator = calc


efermi = calc.get_fermi_level()

potential = calc.get_electrostatic_potential().mean(1).mean(0)
zrange    = np.linspace(0, calc.atoms.cell[2, 2], len(potential), endpoint=False)


fig, ax1 = plt.subplots(figsize=(8,6))


ax1.plot(zrange, potential)
ax1.plot([0, zrange[-1]], [efermi, efermi], label='Fermi level')

n = 6  # get the vacuum level 6 grid-points from the boundary

ax1.plot([0.2, 0.2], [efermi, potential[n]], 'r:')
ax1.text(0.23, (efermi + potential[n]) / 2,
         r'$\phi$ = %.2f eV' % (potential[n] - efermi), va='center')
ax1.plot([zrange[-1] - 0.2, zrange[-1] - 0.2], [efermi, potential[-n]], 'r:')
ax1.text(potential[-1] - 0.23, (efermi + potential[-n]) / 2,
         r'$\phi$ = %.2f eV' % (potential[-n] - efermi),
         va='center', ha='right')

ax1.set_xlabel('$z$, r$\AA$')
ax1.set_ylabel('(Pseudo) electrostatic potential, V')
plt.xlim([0., zrange[-1]])
plt.legend()
plt.show()


