#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 16:01:07 2021

@author: roncofaber
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 15:51:41 2021

@author: roncofaber
"""

import gpaw_tools.FaberPAW as fap
import gpaw
import argparse
import sys, os
#%%
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

#%%

parser = argparse.ArgumentParser()


parser.add_argument("-cn", "--cname", dest="cname",
                    help="choose name output cube file",
                    default="charge_density.cube", type=str)

parser.add_argument("-hn", "--hname", dest="hname",
                    help="choose name output hirshfeld file",
                    default="hirshfeld_charge.xyz", type=str)

parser.add_argument("-hf", "--hirshfeld", dest="hirshfeld", nargs='?',
                    help="Do Hirshfeld analysis",
                    default=False, type=str2bool, const=True)

parser.add_argument("-c", "--cube", dest="cube", nargs='?',
                    help="Print also cube file",
                    default=False, type=str2bool, const=True)

parser.add_argument("-g", "--gridrefinement", dest="gridrefinement", nargs='?',
                    help="grid refinement for cube file",
                    default=4, type=int)

def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)

    system, __ = gpaw.restart(os.getcwd() + "/" + sys.argv[1])

    fap.postprocess_gpw_file(system,
                             do_hirshfeld = attributes.hirshfeld,
                             do_cube      = attributes.cube,
                             cname        = attributes.cname,
                             hname        = attributes.hname,
                             grid         = attributes.gridrefinement,
                             )



if __name__ == '__main__':
    main()
