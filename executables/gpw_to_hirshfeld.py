#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 15:51:41 2021

@author: roncofaber
"""

import gpaw_tools.FaberPAW as fap
import gpaw
import argparse
import sys, os
#%%
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

#%%

parser = argparse.ArgumentParser()


parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="charge_density.cube", type=str)


parser.add_argument("-c", "--cube", dest="cube", nargs='?',
                    help="Print also cube file",
                    default=False, type=str2bool, const=True)


def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)

    system, calc = gpaw.restart(os.getcwd() + "/" + sys.argv[1])

    fap.print_hirshfeld_charges(system,
                                do_cube = attributes.cube,
                                oname   = attributes.outname
                                )



if __name__ == '__main__':
    main()
