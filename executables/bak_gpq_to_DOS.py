#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 20 09:48:05 2021

@author: roncofaber
"""

import gpaw_tools.FaberDOS as fad
import argparse
import os, sys
#%%


parser = argparse.ArgumentParser()


parser.add_argument("-o", "--outname", dest="outname",
                    help="choose name output file",
                    default="default", type=str)

parser.add_argument("-n", "--npoints", dest="npoints",
                    help="choose how many points in the DOS energy line",
                    default=2001, type=int)

parser.add_argument("-l", "--ang_qn", dest="ang_qn",
                    help="choose which angular QN",
                    default="sp", type=str)

parser.add_argument("-w", "--width", dest="width",
                    help="widht of the DOS gaussians",
                    default=0.1, type=float)


def main(argv=sys.argv[2:]):

    attributes = parser.parse_args(argv)


    # transform spdf in 0,1,2,3
    lmom = {
        "s" : 0,
        "p" : 1,
        "d" : 2,
        "f" : 3,
        }

    ang_qn = []
    for ll in attributes.ang_qn:
        if ll in lmom:
            ang_qn.append(lmom[ll])



    fad.myDOS(os.getcwd() + "/" + sys.argv[1],
              npoints  = attributes.npoints,     # number of points in dos
              ang_qn   = ang_qn,   # list of l QN of the system (s=0, p=1, d=2)
              spinpol  = True,     # TODO
              save     = True,     # save or not as pickle
              savename = attributes.outname, # name of the pickle object
              width    = attributes.width
              )

if __name__ == '__main__':
    main()
