#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 13:17:54 2021

@author: roncofaber
"""

import gpaw
import numpy as np
import pickle
# %% FUNCTIONS

# check if list of strings or not
def check_list_of_str(obj):
        return bool(obj) and all(isinstance(elem, str) for elem in obj)

# dummy object that makes a dos entity
class dos_entity():

    def __init__(self):
        return

#%% MAIN CLASS
class myDOS():

    def __init__(self,
                 gpw_file,
                 npoints  = 2001,      # number of points in dos
                 spinpol  = True,      # TODO, for the moment only spinpol calc are allowed
                 save     = True,      # save or not as pickle
                 savename = "default", # name of the pickle object
                 width    = 0.1,       # width of the DOS, if 0 thetra method
                 ):

        # check if it's a new wfs file or a already processed pkl file
        if gpw_file.endswith(".gpw"):
            self.__initialize_dos(gpw_file, npoints, save, savename, width)
        elif gpw_file.endswith(".pkl"):
            self.__restart_dos_from_pickle(gpw_file)


        return

    # read a new gpw restart file and process the DOS
    def __initialize_dos(self, gpw_file, npoints, save, savename, width):

        # save gpw file path (useful for later)
        self.gpw_file = gpw_file

        # set width for dos
        self.width = width

        # set number of points in DOS
        self.npoints = npoints

        if width == 0:
            print("-----------------------------------------")
            print("WARNING: you choose width = 0 which means")
            print("tetrahedron integration will be used and ")
            print("is very slow. Chill and wait in the meantime.")
            print("-----------------------------------------")

        print("Starting to read .gpw file: {}".format(gpw_file))
        # read calculator and generate dos object
        gpaw_calculator = gpaw.GPAW(gpw_file)
        print("File loaded - time to get the DOS")

        # get efermi
        try:
            efermi = gpaw_calculator.get_fermi_level()
            print("Found E_fermi: {}".format(efermi))
            self.n_efermi = [0]

        except:
            efermi = gpaw_calculator.get_fermi_levels()
            print("Found two E_fermis: {} | {}".format(*efermi))
            self.n_efermi = [0,1]

        self.efermi = efermi



        # get total dos
        self.energies, self.tot_dos = self.__calculate_total_dos(gpaw_calculator)
        print("Got total DOS from .gpw file.")

        for ii in self.n_efermi:

            print("Calculated energy interval. Emin: {}, Emax: {}".format(np.min(self.energies[ii]),
                                                                          np.max(self.energies[ii])))

        # get all pdos
        print("Starting to calculate the PDOS for every atom. Hang in there.")
        self.system = self.__calculate_pdos(gpaw_calculator)


        del gpaw_calculator  # remove them after initialization cause they
        # use too much memory

        if save: # save file if requested
            self.__save_self(oname=savename)

        return

    def __restart_dos_from_pickle(self, gpw_file):

        # open previously generated gpw file
        with open(gpw_file, "rb") as fin:
            restart = pickle.load(fin)

        self.__dict__ = restart.__dict__.copy()

        return


    # get total dos
    def __calculate_total_dos(self, gpaw_calculator):

        energies = []
        tot_dos  = []
        for spin in [0, 1]:
            E, dos = gpaw_calculator.get_dos(spin=spin, npts=self.npoints, width=self.width)

            energies.append(E)
            tot_dos.append(dos)

        return np.array(energies), np.array(tot_dos)

    # get all pos from a list of ang QN.
    def __calculate_pdos(self, gpaw_calculator):

        # create system with copy of all atoms and attach lm partial DOS
        system = []
        for cc, atom in enumerate(gpaw_calculator.atoms):

            print("Doing atom #{:>3}: {:2} ".format(cc, atom.symbol), end="", flush=True)

            new_atom = dos_entity()
            new_atom.symbol = atom.symbol
            new_atom.index = cc

            try:
                atom_setup = gpaw.setup_data.SetupData(atom.symbol, "LDA")
            except ValueError:
                print("Invalid symbol or missing setup for that specific atom")

            # get list of n and l quantum numbers
            n_j = atom_setup.n_j
            l_j = atom_setup.l_j

            # iterate over all of 'em
            proj = 0 # projector index
            dos = []
            dos_labels = []
            for nn, ll in zip(n_j, l_j):
                for mm in range(2*ll + 1):
                    if nn == -1:
                        continue #ignore if n = -1

                    # iterate over spin
                    for ss in [0, 1]:

                        # get ldos
                        __, ldos = gpaw_calculator.get_orbital_ldos(
                                                                a = cc,
                                                                spin = ss,
                                                                angular = proj,
                                                                npts = self.npoints,
                                                                width = self.width
                                                               )

                        # append ldos and retrieve label
                        dos.append(ldos)
                        dos_labels.append(
                            self.__assign_pdos_labels(nn, ll, mm, ss))

                        print(".", end="", flush=True)
                    proj += 1

            # append new atom to system
            new_atom.dos = np.array(dos)
            new_atom.dos_labels = dos_labels
            system.append(new_atom)

            print(" done.")


        return system

    # assign labels depending on l, m, s quantum numbers
    def __assign_pdos_labels(self, nqn, lqn, mqn, spin):

        label = {}
        label["n"] = str(nqn)
        label["l"] = ["s", "p", "d", "f"][lqn]
        label["m"] = [[""], ["y", "z", "x"],
                      ["xy", "yz", "z2", "zx", "x2-y2"],
                      ["f1", "f2", "f3", "f4", "f5", "f6", "f7"]][lqn][mqn]
        label["s"] = ["up", "dn"][spin]

        return label

    # convert list of symbols or list of indexes to just a list of indexes
    def __symbols_to_indexes(self, symbols):
            # check if symbols is a list of strings
        if check_list_of_str(symbols):
            if symbols == "all":
                indexes = range(len(self.system))
            else:
                indexes = []
                for atom in self.system:
                    if any([ii == atom.symbol for ii in symbols]):
                        indexes.append(atom.index)
        else:
            indexes = symbols

        indexes = np.array(indexes)

        return indexes

    # group pdos of many atoms, "symbols" can be "all" (all atoms), a list of atom
    # indexes or the symbols of the elements wanted. Return a partial DOS object
    def __group_atoms_pdos(self, symbols="all"):

        # generate index list
        indexes = self.__symbols_to_indexes(symbols)

        # sum relevant pdos
        pdos = np.sum([atom.dos for atom in np.array(self.system)[indexes]], axis=0)
        dos_labels = self.system[indexes[0]].dos_labels

        # create new dos object that is the sum of the atoms requested
        pdos_obj            = dos_entity()
        pdos_obj.dos        = pdos
        pdos_obj.dos_labels = dos_labels

        return pdos_obj

    # group a pdos according to a particular l quantum number. Return a list
    # with the two spin channels
    def __group_l_qn_pdos(self, dosobj, l_qn):

        # spin labels
        s_labels = np.array([ii["s"] for ii in dosobj.dos_labels])
        l_labels = np.array([ii["l"] for ii in dosobj.dos_labels])

        ndos = []
        for spin in ["up", "dn"]:

            indexes = np.where((s_labels == spin) & (l_labels == l_qn))

            # print(indexes)
            ndos.append(np.sum(dosobj.dos[indexes], axis=0))

        return ndos

    def __save_self(self, oname):

        path = "/".join(self.gpw_file.split("/")[:-1]) + "/"

        # check output name
        if oname == "default":
            oname = self.gpw_file.split("/")[-1].split(".")[0] + ".pkl"
        elif not oname.endswith(".pkl"):
            oname = oname.split(".")[0] + ".pkl"


        with open(path + oname, 'wb') as fout:
            pickle.dump(self, fout)

        print("Saved everything as {}".format(oname))
        return


    def get_pdos(self, symbols="all", l_qn="spdf", m_qn="all"):

        # first get pdos obj for all atoms involved
        pdos_obj = self.__group_atoms_pdos(symbols)

        # get pdos of l qn
        pdos = np.sum([self.__group_l_qn_pdos(pdos_obj, ll) for ll in l_qn], axis=0)


        return pdos


